from simulator.PacketSimulator import PacketSimulator
from create_queueing_feasible import create_feasible_v1
from stream_sets.generate_streams import merge_streams_routes
from utils import read_topology
from collections import defaultdict
from collections import OrderedDict
import random
import os
import json
from time import perf_counter
import pickle
import numpy as np
import scipy
from scipy import stats
import matplotlib.pyplot as plt

feasibles_stats = {i * 100: 20 for i in range(1, 11)}


def create_feasibles():
	global feasibles_stats
	topology_path = "../dbg/topology_original.json"
	cut_through = False
	cycle_time = 1000000
	no_late_streams = [17]
	output_path_base = "../simulator_runtime_analysis/"
	for no_streams in feasibles_stats.keys():
		while feasibles_stats[no_streams] > 0:
			seed = random.random()
			create_feasible_v1(topology_path, cut_through, cycle_time, no_streams, no_late_streams, seed, output_path_base)
			old_stream_filename = output_path_base + "streams_feasible.json"
			new_stream_filename = output_path_base + f"streams_{no_streams}_{feasibles_stats[no_streams]}.json"
			os.rename(old_stream_filename, new_stream_filename)
			old_routes_filename = output_path_base + "routes_feasible.json"
			feasibles_stats[no_streams] -= 1
			merge_streams_routes(new_stream_filename, old_routes_filename)


def run_simulator(topology_path, streams_path, cut_through):
	with open(streams_path, "r") as f:
		streams = json.load(f)
	for stream in streams:
		stream['ed'] = 0
	sim = PacketSimulator(read_topology(topology_path), is_cut_trough=cut_through)
	sim.add_packets(streams)
	sim.execute_simulation()


def execute_runtime_eval():
	global feasibles_stats
	runtimes = defaultdict(list)
	for no_streams in feasibles_stats.keys():
		while feasibles_stats[no_streams] > 0:
			stream_path = f"../simulator_runtime_analysis/streams_{no_streams}_{feasibles_stats[no_streams]}.json"
			start = perf_counter()
			try:
				run_simulator("../dbg/topology_original.json", stream_path, False)
			except FileNotFoundError:
				feasibles_stats[no_streams] -= 1
				continue
			runtime = perf_counter() - start
			runtimes[no_streams].append(runtime)
			feasibles_stats[no_streams] -= 1
	with open("runtimes.bin", "wb") as f:
		pickle.dump(runtimes, f)


def mean_confidence_interval(data, confidence=0.9):
	print(data)
	a = 1.0 * np.array(data)
	n = len(a)
	m, se = np.mean(a), scipy.stats.sem(a)
	h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
	return m, h


def plot_runtimes():
	with open("runtimes.bin", "rb") as f:
		runtimes = pickle.load(f)
	runtimes = OrderedDict(runtimes)
	runtimes_mean_conf = OrderedDict()
	for no_streams in runtimes.keys():
		m, h = mean_confidence_interval(runtimes[no_streams])
		runtimes_mean_conf[no_streams] = (m, h)
	fig, ax = plt.subplots()
	# ax.plot(runtimes_mean_conf.keys(), [m for m, h in runtimes_mean_conf.values()])
	ax.errorbar(runtimes_mean_conf.keys(), [m for m, h in runtimes_mean_conf.values()], yerr=[h for m, h in runtimes_mean_conf.values()], color="black")
	axis_font = {'size': 25}
	ax.set_xlabel("Size of Stream Set [Streams]", usetex=True, **axis_font)
	ax.set_ylabel("Simulation Runtime [s]", usetex=True, **axis_font)
	axis_font = {'size': 30}
	ax.set_title("Simulation Runtime vs Size of Stream Set", usetex=True, **axis_font)
	ax.tick_params(axis='both', labelsize=18)
	plt.show()



if __name__ == '__main__':
	# create_feasibles()
	# run_simulator("../dbg/topology_original.json", "../simulator_runtime_analysis/streams_100_20.json", False)
	# 	execute_runtime_eval()
	plot_runtimes()