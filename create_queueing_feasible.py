#!/usr/bin/env python3

import argparse
import json
import pickle
import random
from copy import deepcopy
from math import ceil
from os.path import join
from graph_tool.topology import Graph

from SimulationWrapper import get_late_streams
from simulator.PacketSimulator import PacketSimulator
from converter_files.Schedule2SimConversion import generate_flow_xml
from converter_files.Schedule2SimConversion import generate_schedule_xml
from converter_files.Schedule2SimConversion import generate_topology_graphml
from converter_files.Schedule2SimConversion import generate_scheduler_requirements
from converter_files.Schedule2SimConversion import generate_d3_files
from converter_files.Schedule2SimConversion import generate_jrs_requirements
from topology.topology import parse_topology
from tracing_based_scheduling_converter import rename_topology_attributes
from tsnutils.streams.generate import generate_streams


def create_feasible_v1(topology_path='dbg/topology.json', cut_through=False, cycle_time_ns=1000000, no_streams=100,
                       no_late_streams=1, seed=8621811928342138509, output_path='dbg/', seed_state=None):
    # init
    topology = parse_topology(topology_path)
    safety_factor = 1.1
    tmp_path = join(output_path, "feasible_tmp.json")
    if not seed_state:
        random.seed(seed)
    else:
        random.setstate(seed_state)
    streams_sol = {}
    streams_input = {}
    # to_remove = range(0, no_streams)
    while 1:
        # get random streams, pass and get seed states
        current_random_state = random.getstate()
        streams, current_random_state = generate_streams(no_streams - len(streams_sol), cycle_time_ns, None,
                                                         tmp_path, seed_state=current_random_state, topology=topology)
        random.setstate(current_random_state)
        for stream_id, stream in streams.items():
            # la is path delay without queueing
            upper_limit = cycle_time_ns - stream.max_delay
            lower_limit = 0
            # set earliest departure randomly
            if random.getrandbits(1) == 0:
                stream.state['ed'] = random.randrange(lower_limit, upper_limit, 1000)
            else:
                stream.state['ed'] = 0
            # stream.id = to_remove[stream_id]
            streams_sol[stream_id] = stream
            # need separate stream input set to check whether streams arrive late when ed=0
            stream_input = deepcopy(stream)
            stream_input.state['ed'] = 0
            streams_input[stream_id] = stream_input

        # do simulation on solution set to get deadlines
        simulator = PacketSimulator(topology, is_cut_trough=cut_through, verbose=False)
        simulator.add_packets(streams_sol)
        simulator.execute_simulation()
        with open("packet_history_dict.bin", 'rb') as pickle_file:
            packet_history_dict_sol = pickle.load(pickle_file)
        # set simulated stream arrival time as la
        stream_arrives_after_cycle_flag = False
        stream_arrives_after_cycle_idx = []
        for simulated_stream_idx, simulated_stream in packet_history_dict_sol.items():
            last_event_ts, last_event = simulated_stream[-1]
            if int(1000 * ceil(last_event_ts / 1000)) > cycle_time_ns:
                stream_arrives_after_cycle_flag = True
                stream_arrives_after_cycle_idx.append(simulated_stream_idx)
            # raise ValueError('Stream arrives after cycle finishes!')
            last_event_ts = int(1000 * ceil(last_event_ts * safety_factor / 1000))
            streams_sol[simulated_stream_idx].state['la'] = last_event_ts
            streams_sol[simulated_stream_idx].max_delay = last_event_ts
            streams_input[simulated_stream_idx].state['la'] = last_event_ts
            streams_input[simulated_stream_idx].max_delay = last_event_ts
        # do simulation on input set
        simulator = PacketSimulator(topology, is_cut_trough=cut_through, verbose=False)
        simulator.add_packets(streams_input)
        simulator.execute_simulation()
        with open("packet_history_dict.bin", 'rb') as pickle_file:
            packet_history_dict_input = pickle.load(pickle_file)
        # check if stream requirements adhered to
        late_stream_sol, late_stream_flag_sol, log_sol = get_late_streams(streams_sol, packet_history_dict_sol)
        late_stream_input, late_stream_flag_input, log_input = get_late_streams(streams_input,
                                                                                packet_history_dict_input)
        # check late stream requirements
        if stream_arrives_after_cycle_flag:
            to_remove = stream_arrives_after_cycle_idx
        elif late_stream_flag_sol:
            # solution stream set should not have any late streams
            raise ValueError("Solution stream set should not have any late streams!")
        elif not late_stream_flag_sol and not late_stream_flag_input:
            to_remove = [random.choice(list(packet_history_dict_sol.keys()))]
        elif not late_stream_flag_sol and late_stream_flag_input:
            # found a set where solution exists and input has late streams
            if len(late_stream_input) in no_late_streams:
                # solution has required number of late sreams, break
                break
            elif all(len(late_stream_input) > i for i in no_late_streams):
                # solution has more late streams than required, remove one late stream
                to_remove = [random.choice(list(late_stream_input))]
            else:
                # solution has less late streams than required, remove a stream which is not late
                to_remove = [random.choice(list(set(packet_history_dict_input.keys()) - late_stream_input))]
        streams_sol = {stream_id: stream for stream_id, stream in streams_sol.items() if stream_id not in to_remove}
        streams_input = {stream_id: stream for stream_id, stream in streams_input.items() if stream_id not in to_remove}
    # generate log and summary files
    summary_json = {
        'late_streams': list(late_stream_input),
        'summary_input': log_input,
        'summary_sol': log_sol,
        'packet_history_dict_sol': packet_history_dict_sol,
        'packet_history_dict_input': packet_history_dict_input
    }
    with open(join(output_path, "summary.json"), "w") as f:
        json.dump(summary_json, f)
    log = {
        'summary_input': log_input,
        'summary_sol': log_sol
    }
    with open(join(output_path, "log.json"), "w") as f:
        json.dump(log, f)
    # do conversion files (for nesting), they need to be renamed
    # topology_renamed, streams_sol_renamed, packet_history_dict_sol_renamed = _rename_nodes(Graph(topology),
    #                                                                                        deepcopy(streams_sol),
    #                                                                                        packet_history_dict=deepcopy(
    #                                                                                            packet_history_dict_sol))
    # schedule2simuation gen need topology attributes in CamelCase
    # topology_renamed = rename_topology_attributes(topology_renamed, output_file=False)
    generate_topology_graphml(Graph(topology), join(output_path, "topology.graphml"))
    generate_flow_xml(topology, deepcopy(streams_sol), join(output_path, "flows.xml"))
    generate_schedule_xml(Graph(topology), deepcopy(packet_history_dict_sol), stream.cycle_time,
                          join(output_path, "schedule.xml"))
    # generate d3 files
    generate_d3_files(Graph(topology), deepcopy(packet_history_dict_sol), deepcopy(streams_sol),
                      output_path)
    # generate jrs requirements
    generate_jrs_requirements(deepcopy(streams_sol), output_path)
    # do scheduler requirements input files (JSSP, tracing scheduler)
    generate_scheduler_requirements(deepcopy(streams_sol), output_path)

    return len(late_stream_input)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Add a feasible with queueing task to the DB. Prints the ID of the inserted entry.")
    parser.add_argument('--topology-path', type=str, help="Topology ID to use", default='tsnutils/examples/topology.json')
    parser.add_argument('--cut-through', action="store_true",
                        help="If true, the feasible will be generated by a network simulator using the cut through option in switches.",
                        default=False)
    parser.add_argument('--cycle', type=int, default=3000000,
                        help="Cycle to use (unit: nanoseconds), default: 1000000.")
    parser.add_argument('--flows', type=int,
                        help="Number of flows in feasible.", default=100)
    parser.add_argument('--no-late-flows', type=int,
                        help="Number of flows arriving after their deadline.", default=1)
    parser.add_argument('--seed', type=int,
                        help="Seed to use when generating feasible.", default=8621811928342138509)
    parser.add_argument('--output', type=str,
                        help="Path to the directory where output data will be saved.", default='dbg/')
    args = parser.parse_args()

    create_feasible_v1(args.topology_path, args.cut_through, args.cycle, args.flows, [args.no_late_flows], args.seed,
                       args.output)
