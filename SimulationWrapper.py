#!./venv/bin/python3
import json
import pickle
from copy import deepcopy
from os import path
from time import perf_counter
from anytree import AnyNode, RenderTree, render
import numpy as np
import random

from converter_files.Schedule2SimConversion import generate_flow_xml
from converter_files.Schedule2SimConversion import generate_schedule_xml
from converter_files.Schedule2SimConversion import generate_topology_graphml
from exceptions import NoSolutionException
from exceptions import SchedulingMethodNotFound
from simulator.PacketSimulator import PacketSimulator
from stream_sets.generate_streams import build_conflict_graph
from stream_sets.generate_streams import get_conflict_streams_link_time
from stream_sets.generate_streams import get_conflict_streams_naive
from stream_sets.generate_streams import sum_remaining_time
from utils import render_tree

from tsnutils.topology import topology
from tsnutils.streams import stream as tsnutils_stream


class TracingScheduling:
    def __init__(self, eval_output_path, topology, streams, run_time, cut_through,
                 rescheduling_method='naive', xml_directory='converter_files', log=False, output=True):
        """
        Runs the rescheduling algorithm
        :param eval_output_path: Path where evaluation log is written to
        :param topology_path: Path to topology file. Possible file types: {*.graphml | *.json}
        :param streams_path: Path to streams.json
        :param run_time: Maximum runtime of algorithm [s]
        :param cut_through: is simulator in cut through mode
        :param rescheduling_method: Rscheduling method to use. Possible values: {naive|link_time_rem_time|link_time_ls|link_time_mcts}
        :param xml_directory: Directory where xml files are written to.
        :param log: Write a log file to eval_output path
        :param output: Toggle output of scheduler (xml files as well as log files)
        :return:
        """
        # init
        self.eval_output_path = eval_output_path
        self.run_time = run_time
        self.cut_through = cut_through
        self.rescheduling_method = rescheduling_method
        self.log = log
        self.output = output
        self.topology = topology
        self.streams = streams

        # TODO: [Alex] Initialize with reasonable values
        for stream in self.streams.values():
            stream.state['la'] = 0
            stream.state['ed'] = 0

        self.sim_output = {}
        # tree of reschedule_conflicting_streams_link_time
        self.rescheduling_link_time_tree = None
        # best node in the search tree found by link time rescheduling algorithm (best means: least amount of late
        # streams with least amount of remaining time to deadline as tiebraker
        self.best_tree_node = None
        # converter file names
        self.toplogy_graphml = path.join(xml_directory, "topology.graphml")
        self.flow_xml = path.join(xml_directory, "flows.xml")
        self.schedule_xml = path.join(xml_directory, "schedule.xml")

    def run_scheduling(self):
        # time measurement
        t_start = perf_counter()
        t_end = t_start + self.run_time
        # actual scheduling
        while perf_counter() < t_end:
            # do simulation
            simulator = PacketSimulator(self.topology, is_cut_through=self.cut_through)
            simulator.add_packets(self.streams)
            simulator.execute_simulation()
            # get simulation results
            with open("packet_history_dict.bin", 'rb') as pickle_file:
                packet_history_dict = pickle.load(pickle_file)
            # check if stream requirements adhered to
            late_streams_idx, late_stream_flag, _ = get_late_streams(self.streams, packet_history_dict)
            # get new earliest departure
            try:
                if self.rescheduling_method == "naive":
                    self.reschedule_conflicting_streams_naive(late_streams_idx, packet_history_dict)
                elif self.rescheduling_method == "link_time_ls" or self.rescheduling_method == "link_time_rem_time":
                    self.reschedule_conflicting_streams_link_time(late_streams_idx, packet_history_dict)
                else:
                    raise SchedulingMethodNotFound
            except NoSolutionException:
                print("No solution found for stream set {}".format(self.streams_path))
                break
            finally:
                # save results
                t_schedule = perf_counter() - t_start
                if self.rescheduling_method == "naive":
                    self.sim_output[t_schedule] = [list(late_streams_idx), t_schedule]
                elif self.rescheduling_method == "link_time_ls" or self.rescheduling_method == "link_time_rem_time":
                    self.sim_output[t_schedule] = [list(self.best_tree_node.late_streams), t_schedule]
                else:
                    raise SchedulingMethodNotFound
                if not late_stream_flag:
                    break
        # print results
        if self.output:
            print_output(self.sim_output, self.eval_output_path, self.log, self.best_tree_node.packet_history_dict)
            self.print_search_tree()
            render_tree(self.rescheduling_link_time_tree.root)
            generate_topology_graphml(self.topology, self.toplogy_graphml)
            generate_flow_xml(self.topology, self.best_tree_node.stream_set, self.flow_xml)
            generate_schedule_xml(self.topology, self.best_tree_node.packet_history_dict,
                                  self.best_tree_node.stream_set[0].cycle_time, self.schedule_xml)

    def export_result_stream_set(self):
        return self.best_tree_node.stream_set, self.best_tree_node.late_streams

    def reschedule_conflicting_streams_naive(self, late_streams_idx, packet_history_dict):
        """
        Naive approach to rescheduling late arriving streams. Finds streams that conflict with late arriving streams and
        moves them back.
        :param late_streams_idx: Indices of streams arriving after latest arrival
        """
        parent = self.rescheduling_link_time_tree
        self.rescheduling_link_time_tree = AnyNode(stream_set=deepcopy(self.streams),
                                                   rem_time=sum_remaining_time(self.streams, packet_history_dict,
                                                                               late_streams_idx),
                                                   late_streams=late_streams_idx, parent=parent,
                                                   packet_history_dict=packet_history_dict, conflict_graph=None)
        self.update_best_streams_set()
        mapping = get_conflict_streams_naive(self.streams, late_streams_idx)
        conflict_streams_idx = set([idx for sublist in mapping.values() for idx in sublist])
        for idx in conflict_streams_idx:
            self.streams[idx]['ed'] += 1000

    def reschedule_conflicting_streams_link_time(self, late_streams_idx, packet_history_dict):
        parent = self.rescheduling_link_time_tree
        streams_copy = self.streams
        # append new node
        self.rescheduling_link_time_tree = AnyNode(stream_set=deepcopy(self.streams),
                                                   rem_time=sum_remaining_time(self.streams, packet_history_dict,
                                                                               late_streams_idx),
                                                   late_streams=late_streams_idx, parent=parent,
                                                   packet_history_dict=packet_history_dict, conflict_graph=None,
                                                   mapping=None, times=None)
        self.update_best_streams_set()
        if not late_streams_idx:
            return self.streams
        # traverse tree
        while self.link_time_condition():
            if self.rescheduling_link_time_tree.parent is None:
                # if parent is None we arrived at the root, no solution could be found
                raise NoSolutionException('No solution found!')
            # get parent attributes
            self.rescheduling_link_time_tree = self.rescheduling_link_time_tree.parent
            self.streams = deepcopy(self.rescheduling_link_time_tree.stream_set)
            late_streams_idx = self.rescheduling_link_time_tree.late_streams
            packet_history_dict = self.rescheduling_link_time_tree.packet_history_dict
        # get conflict graph
        if self.rescheduling_link_time_tree.conflict_graph is None:
            # get conflicts and times
            mapping, times = get_conflict_streams_link_time(self.streams, late_streams_idx, packet_history_dict)
            conflict_graph = build_conflict_graph(mapping, self.streams)
            self.rescheduling_link_time_tree.conflict_graph = conflict_graph
            self.rescheduling_link_time_tree.mapping = mapping
            self.rescheduling_link_time_tree.times = times
        else:
            conflict_graph = self.rescheduling_link_time_tree.conflict_graph
            mapping = self.rescheduling_link_time_tree.mapping
            times = self.rescheduling_link_time_tree.times
        # TODO compare conflict graphs, new conflict graph should have fewer conflicts since stream was pushed back
        # find streams with maximum conflicts
        vertices = list(conflict_graph.vertices())
        conflict_degrees = [v.in_degree() for v in vertices]
        max_conflicts_stream_idx = self.choose_max_conflict_stream_idx(conflict_degrees, times, vertices)
        if max_conflicts_stream_idx == -1:
            return
        # max_conflicts_stream_idx = int(vertices[argmax([v.in_degree() for v in vertices])])
        # filter conflict stream out
        conflict_graph.vertex_properties['not_removed'][max_conflicts_stream_idx] = False
        conflict_graph.set_vertex_filter(conflict_graph.vertex_properties['not_removed'])
        max_conflicts_stream = self.streams[max_conflicts_stream_idx]
        # get overlap for max conflict stream
        overlaps = [overlap for late_stream_idx in mapping.keys() for idx, overlap in times[late_stream_idx] if
                    idx == max_conflicts_stream_idx]
        if not overlaps:
            # raise ValueError("Overlap List is empty!")
            return
        max_overlap = max(overlaps)
        # add to overlap so that conflict stream queues after late stream for sure!
        if max_overlap == 0:
            max_overlap += 1000
        elif max_overlap > 0:
            max_overlap += 1
        """
        Only add to ed, if overlap is positive - this means, that late stream arrives later than conflict stream and is
        queued after conflict stream. If overlap is negative, late stream arrives before conflict stream and is queued
        earlier. Adjusting would not be good in this case.
        """
        if max_overlap > 0:
            max_conflicts_stream['ed'] += max_overlap

    def choose_max_conflict_stream_idx(self, conflict_degrees, times, vertices):
        tmp = conflict_degrees == np.max(conflict_degrees)
        max_conflict_streams_idxs = [int(vertices[s]) for s in
                                     np.argwhere(conflict_degrees == np.max(conflict_degrees)).flatten().tolist()]
        # get conflict pairs for streams in max conflict streams
        times_flattened = [conflict_pair for conflict_pairs in times.values() for conflict_pair in conflict_pairs if
                           (conflict_pair[0] in max_conflict_streams_idxs) and conflict_pair[0] not in times.keys()]
        # return conflict streams which has largest overlap
        try:
            return max(times_flattened, key=lambda x: x[1])[0]
        except ValueError:
            # if we end up here, the late streams have no conflicts - retun to previous node
            return -1

    def update_best_streams_set(self):
        if self.best_tree_node is None:
            self.best_tree_node = self.rescheduling_link_time_tree
        elif len(self.best_tree_node.late_streams) > len(self.rescheduling_link_time_tree.late_streams):
            self.best_tree_node = self.rescheduling_link_time_tree
        elif len(self.best_tree_node.late_streams) == len(self.rescheduling_link_time_tree.late_streams) \
                and self.best_tree_node.rem_time < self.rescheduling_link_time_tree.rem_time:
            self.best_tree_node = self.rescheduling_link_time_tree

    def link_time_condition(self):
        parent_exists = self.rescheduling_link_time_tree.parent is not None
        conflict_graph_exists = self.rescheduling_link_time_tree.conflict_graph is not None

        if parent_exists:
            dbg_smaller = self.rescheduling_link_time_tree.rem_time <= self.rescheduling_link_time_tree.parent.rem_time
            dbg_rem_time_parent = self.rescheduling_link_time_tree.parent.rem_time
            dbg_rem_time = self.rescheduling_link_time_tree.rem_time
            dbg_ls_parent = len(self.rescheduling_link_time_tree.parent.late_streams)
            dbg_ls = len(self.rescheduling_link_time_tree.late_streams)

        # remember, remaining time is negative! Smaller values means more accumulated deadline miss time
        child_rem_time_smaller_than_parent_rem_time = parent_exists and self.rescheduling_link_time_tree.rem_time <= self.rescheduling_link_time_tree.parent.rem_time
        parent_less_streams_than_child = parent_exists and len(
            self.rescheduling_link_time_tree.parent.late_streams) < len(
            self.rescheduling_link_time_tree.late_streams)
        # tiebraker if parent and child have equal amount of late streams
        if parent_exists and not parent_less_streams_than_child and len(
                self.rescheduling_link_time_tree.parent.late_streams) == len(
                self.rescheduling_link_time_tree.late_streams):
            parent_less_streams_than_child = child_rem_time_smaller_than_parent_rem_time
        late_streams = set(self.rescheduling_link_time_tree.late_streams)
        conflict_edges_sources = set()
        if conflict_graph_exists:
            conflict_edges_sources = set([e.target() for e in self.rescheduling_link_time_tree.conflict_graph.edges()])
        conflict_graph_has_no_conflicts = conflict_graph_exists and late_streams == conflict_edges_sources

        if self.rescheduling_method == 'link_time_ls':
            # less streams condition with remaining time as tiebraker
            condition = parent_less_streams_than_child or conflict_graph_has_no_conflicts
        elif self.rescheduling_method == 'link_time_rem_time':
            # remaining time condition
            condition = child_rem_time_smaller_than_parent_rem_time or conflict_graph_has_no_conflicts
        else:
            raise SchedulingMethodNotFound

        return condition

    def print_search_tree(self):
        print(RenderTree(self.rescheduling_link_time_tree.root, style=render.DoubleStyle).by_attr("rem_time"))


def get_late_streams(streams, packet_history_dict):
    late_streams_idx = set()
    late_stream_flag = False
    to_return = ""
    for s_id, stream in streams.items():
        arrival_time = packet_history_dict[s_id][-1][0]  # get arrival time of last event in packet history
        deadline = stream.max_delay
        if arrival_time > deadline:
            late_streams_idx.add(s_id)
            to_print = "Stream {} arrived at {}, latest arrival is {}!".format(s_id, arrival_time, deadline)
            print(to_print)
            to_return += "\n" + to_print
            late_stream_flag = True
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    return late_streams_idx, late_stream_flag, to_return


def print_output(sim_output, eval_output_path, log, best_packet_history_dict):
    """
    Prints summary of simulation runs.
    """
    last_iter = 0
    for iteration, result in sim_output.items():
        late_streams_string = ', '.join(str(late_stream) for late_stream in result[0])
        print(
            "Iteration {}: {} late streams in {} - ".format(iteration, len(result[0]), result[1]) + late_streams_string)
        last_iter = iteration
    sim_output['packet_history_dict'] = best_packet_history_dict
    if eval_output_path:
        if not log:
            # written as eval file for local evaluation
            with open(eval_output_path, "w") as f:
                json.dump(sim_output, f, indent=2)
        else:
            # written as log file for scheduling-benchmarking tool
            sim_output['success'] = (len(sim_output[last_iter][0]) == 0)
            with open(path.join(eval_output_path, "log"), "w") as f:
                json.dump(sim_output, f, indent=2)


def verify_simulator(verification_id: int):
    """
    Runs the verfication test cases.
    :param verification_id: Which test case to execute
    """
    topology_path = 'cut_through_verific/test_topology_{}.json'.format(verification_id)
    streams_path = 'cut_through_verific/test_streams_{}.json'.format(verification_id)

    # do simulation
    simulator = PacketSimulator(topology_path, streams_path, is_cut_trough=False)
    simulator.execute_simulation()


if __name__ == '__main__':
    s = TracingScheduling("test_result.json", "tsnutils/examples/topology.json", "tsnutils/examples/streams.json",
                          20, False, rescheduling_method="link_time_rem_time")
    # s = TracingScheduling("test_result.json", "dbg/topology.json", "dbg/streams_feasible.json", 120, False,
    # 					  rescheduling_method="link_time_rem_time")
    # s = TracingScheduling("dbg/", "stream_sets/topology.json", "dbg/streams.json",
    # 					  60, False, rescheduling_method="link_time_ls", log=True)
    s.run_scheduling()
