#!/usr/bin/env python3
"""
This file adds a CLI to SimulationWrapper.
"""
import argparse

from MCTS import MCTS
from SimulationWrapper import TracingScheduling


def execute_scheduling(argmnts):
	if argmnts.rescheduling_method in ['naive', 'link_time_ls', 'link_time_rem_time']:
		s = TracingScheduling(argmnts.eval_output_path, argmnts.topology_path, argmnts.streams_path, argmnts.runtime,
							  argmnts.cut_through, rescheduling_method=argmnts.rescheduling_method,
							  xml_directory=argmnts.xml_directory, log=True)
		s.run_scheduling()
	elif argmnts.rescheduling_method in ['link_time_mcts']:
		mcts = MCTS(argmnts.eval_output_path, argmnts.topology_path, argmnts.streams_path, argmnts.runtime,
					argmnts.cut_through, argmnts.xml_directory, log=True)
		mcts.export_solution()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--eval_output_path", type=str,
						help="Path to where evaluation results are written. If empty, no eval results are written.")
	parser.add_argument("--topology_path", type=str, required=True,
						help="Path to topology file.")
	parser.add_argument("--streams_path", type=str, required=True,
						help="Path to streams.json file.")
	parser.add_argument("--runtime", type=int, required=True,
						help="Maximum runtime [s] of the scheduler.")
	parser.add_argument("--cut_through", default=False, action='store_true',
						help="Cut through option of simulator.")
	parser.add_argument("--rescheduling_method", type=str, required=True,
						help="Scheduling method the scheduler uses.", choices=['naive', 'link_time_ls',
																			   'link_time_rem_time', 'link_time_mcts'])
	parser.add_argument("--xml_directory", type=str,
						help="Directory were converter files are written to.")
	args = parser.parse_args()
	# args = argparse.Namespace(cut_through=False, eval_output_path='dbg/', rescheduling_method='link_time_mcts', runtime=60, streams_path='dbg/streams.json', topology_path='dbg/topology.json', xml_directory='converter_files/')

	execute_scheduling(args)
