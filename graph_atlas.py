import argparse
import json

import matplotlib.pyplot as plt
import networkx as nx
import random


def edge_core_graph(n=50, p_add_edge=0.33, add_edge_node_degree=2, add_edge_path_len=4):
    G = nx.random_tree(n)

    non_leaf_nodes = set(node for node, degree in G.degree() if add_edge_node_degree <= degree)

    for node in non_leaf_nodes:
        if random.random() > p_add_edge:
            rewiring_candidates = non_leaf_nodes.copy().difference({node})
            path_len_root = len(nx.shortest_path(G, 0, node))
            rewiring_candidates = set(filter(
                lambda candidate: abs(len(nx.shortest_path(G, 0, candidate)) - path_len_root) < add_edge_path_len,
                rewiring_candidates))

            G.add_edge(node, random.sample(rewiring_candidates, 1)[0])

    return G


def two_porter_ring(n):
    G = nx.cycle_graph(n)

    nodes = list(G.nodes)

    for ring_node in nodes:
        node_id = len(G.nodes)
        G.add_node(node_id)
        G.add_edge(ring_node, node_id)

    return G


def ring(n, global_no_switches):
    G = nx.cycle_graph(n)

    G = nx.relabel_nodes(G, lambda label: "s" + str(label + global_no_switches))

    return G


def two_porter_line(n):
    G = nx.Graph()
    # add switches on line
    G.add_node('s0')
    line_nodes = ['s0']
    for number in range(1, n):
        line_node = 's' + str(number)
        line_nodes.append(line_node)
        G.add_node(line_node)
        G.add_edge(line_nodes[number - 1], line_node)

    # add hosts
    for idx, line_node in enumerate(line_nodes):
        node_id = 'h' + str(idx)
        G.add_node(node_id)
        G.add_edge(line_node, node_id)

    return G


def hierarchical_rings_rec(n=0, level_rings=[(1, 4), (4, 3)], level=0, no=0):
    print(level_rings)
    amount, nodes = level_rings[0]
    # don't know what amount does
    # nodes = number of nodes for current ring
    level_rings.remove((amount, nodes))

    G = ring(nodes, n)
    # increment global number of switches to name nodes globally uniquely
    n += nodes
    # ring id to distinguish nodes from different rings
    for node in G.nodes:
        G.nodes[node]['gw_cluster_id'] = no
    no += 1
    # G = nx.relabel_nodes(G, lambda label: '{}_{}_{}_{}'.format(random.randrange(0, 1000), level, no, label))

    if len(level_rings) > 0:
        # number of lower rings
        no_lower_nodes = level_rings[0][0]
        # number of nodes in lower rings
        no_lower_nodes_nodes = level_rings[0][1]

        # number of lower rings can't be higher than number of nodes in current ring
        if nodes < no_lower_nodes:
            print('Error not enough higher levels gateways!', nodes, no_lower_nodes)
            return -1

        # current ring nodes
        higher_level_gw = list(G.nodes)[-no_lower_nodes:]

        lower_rings = []

        for i in range(no_lower_nodes):
            lower_rings.append(hierarchical_rings_rec(n=n, level_rings=level_rings.copy(), level=level + 1, no=no))
            # increment global number of nodes
            n += no_lower_nodes_nodes
            no += 1

        for lower_ring in lower_rings:
            if lower_ring != -1:
                print(lower_ring.nodes)
                G = nx.union(G, lower_ring)
                # need to connect via switch to higher ring, ot via host
                lower_ring_node = random.choice(list(lower_ring.nodes))
                while lower_ring.degree[lower_ring_node] <= 1:
                    lower_ring_node = random.choice(list(lower_ring.nodes))
                #  Remove host on gateway node
                G.remove_node('h' + lower_ring_node[1:])
                G.add_edge(higher_level_gw.pop(), lower_ring_node)

    else:
        no = no - 1
        # if this is lowest ring add host nodes
        nodes = list(G.nodes)
        for ring_node in nodes:
            name = "h" + ring_node.strip('s')
            G.add_edge(ring_node, name)
            G.nodes[name]['gw_cluster_id'] = no
    return G


def grid(n, m):
    G = nx.grid_2d_graph(n, m)

    label_map = dict(zip(G.nodes, range(len(G.nodes))))
    G = nx.relabel_nodes(G, label_map)
    edge_nodes = set(node for node, degree in G.degree() if degree <= 3)

    for edge_node in edge_nodes:
        node_id = len(G.nodes)
        G.add_node(node_id)
        G.add_edge(node_id, edge_node)

    return G


def hierarchical_hybrid_graph(tree_n, backbone_n, min_size, max_size):
    # Create balanced tree
    base_graph = nx.balanced_tree(tree_n, 2)

    max_degree = max([degree for node, degree in base_graph.degree()])
    # Select nodes for thinning out balanced tree
    remove_nodes = []
    for i in range(max_degree - 1):
        for node in base_graph.nodes:
            if base_graph.degree(node) == i:
                if random.random() < 0.75:
                    remove_nodes.append(node)

    # Remove selected nodes (thinning out)
    base_graph.remove_nodes_from(remove_nodes)

    # Contains unconnected subtopologies
    graphs = {}
    for node, degree in sorted(base_graph.degree()):
        # All nodes without leafs
        if degree > 1:
            no_neighbors = len(list(base_graph.neighbors(node)))
            # Create Backbone
            if node == 0:
                tmp_graph = nx.cycle_graph(max(backbone_n, 2 * no_neighbors + 1))
            # Create subring
            else:
                tmp_graph = nx.cycle_graph(
                    random.choice(range(max(min_size, 2 * no_neighbors), max(max_size, 2 * no_neighbors + 1))))
            tmp_graph = nx.relabel_nodes(tmp_graph, lambda old: '{}-{}'.format(node, old))
            graphs[node] = ('ring', tmp_graph)
        # All leafs
        else:
            # Leafs can be either ring or line
            # Ring
            if random.getrandbits(1):
                tmp_graph = nx.cycle_graph(random.choice(range(min_size, max_size)))
                tmp_graph = nx.relabel_nodes(tmp_graph, lambda old: '{}-{}'.format(node, old))
                graphs[node] = ('ring', tmp_graph)
            # Line
            else:
                tmp_graph = nx.path_graph(random.choice(range(min_size, max_size)))
                tmp_graph = nx.relabel_nodes(tmp_graph, lambda old: '{}-{}'.format(node, old))
                graphs[node] = ('line', tmp_graph)

    # Create new graph for final network
    G = nx.Graph()

    # Gateway Cluster-ID
    nx.set_edge_attributes(G, -1, 'gw_cluster_id')
    nx.set_node_attributes(G, -1, 'gw_cluster_id')
    gw_cluster_id = 0

    # Iterate base graph to create connections and gateways
    dfs_predecessors = nx.dfs_predecessors(base_graph)
    for dfs_node in nx.dfs_preorder_nodes(base_graph):

        tmp_graph = graphs[dfs_node][1]
        if dfs_node in dfs_predecessors.keys():
            predecessor = dfs_predecessors[dfs_node]
            # Merge subtopology into final graph
            G = nx.union(G, tmp_graph)

            # Select gateways
            gateways = [node for node in graphs[predecessor][1] if G.degree(node) <= 2]
            if graphs[dfs_node][0] == 'ring':
                gws0 = list(gateways)[:2]
                gws1 = list(tmp_graph.nodes())[-2:]

                for gw0, gw1 in zip(gws0, gws1):
                    G.nodes[gw0]['gw_cluster_id'] = gw_cluster_id
                    G.nodes[gw1]['gw_cluster_id'] = gw_cluster_id
                    G.add_edge(gw0, gw1, gw_cluster_id=gw_cluster_id)
                gw_cluster_id += 1

            else:
                gw0 = list(gateways)[:1][0]
                G.nodes[gw0]['gw_cluster_id'] = gw_cluster_id
                gw1 = list(tmp_graph.nodes())[-1:][0]

                G.add_edge(gw0, gw1, gw_cluster_id=gw_cluster_id)
                gw_cluster_id += 1

        else:
            G = tmp_graph

    for node in [node for node, degree in G.degree() if degree <= 2]:
        leaf = len(G.nodes())
        G.add_node(leaf)
        G.add_edge(node, leaf)

    G = nx.relabel_nodes(G, dict(zip(list(G.nodes), map(lambda x: 'h' + str(x), range(G.number_of_nodes())))))

    return G, base_graph


def write_to_json(G, output_file):
    vertices = {}
    edges = {}

    for v0, v1 in G.edges():
        if v0 not in vertices.keys():
            vertices[v0] = {'name': str(v0), 'processing_delay': 2000,
                            'gw_cluster_id': G.nodes[v0]['gw_cluster_id']
                            if 'gw_cluster_id' in G.nodes[v0] else -1,
                            'IsSwitch': True if v0.startswith('s') else False}
        if v1 not in vertices.keys():
            vertices[v1] = {'name': str(v1), 'processing_delay': 2000,
                            'gw_cluster_id': G.nodes[v1]['gw_cluster_id']
                            if 'gw_cluster_id' in G.nodes[v1] else -1,
                            'IsSwitch': True if v1.startswith('s') else False}

        edges['_'.join([str(v0), str(v1)])] = {'source': str(v0), 'target': str(v1), 'link_speed': 1000,
                                               'propagation_delay': 0,
                                               'gw_cluster_id': G.edges[(v0, v1)]['gw_cluster_id']
                                               if 'gw_cluster_id' in G.edges[(v0, v1)] else -1}

    with open(output_file, 'w') as output_fd:
        json.dump({'edges': edges, 'vertices': vertices}, output_fd)


def main(args):
    if args.edge_core:
        if args.nodes:
            G = edge_core_graph(n=args.nodes)
        else:
            G = edge_core_graph()
    elif args.factory_backbone:
        G, base_graph = hierarchical_hybrid_graph(5, 15, 5, 10)
    elif args.ring:
        G = two_porter_ring(25)
    elif args.hierarchical_rings_rec:
        G = hierarchical_rings_rec()
    elif args.line:
        G = two_porter_line(args.nodes)

    print('Nodes:', G.number_of_nodes())
    print('Edges:', G.number_of_edges())

    # pos = nx.spring_layout(G)
    # nx.draw(G, pos)
    # plt.show()

    if not args.output_file:
        for edge in G.edges():
            print(edge[0], edge[1])
    else:
        if not args.line_graphml:
            write_to_json(G, args.output_file)
        else:
            nx.write_graphml(G, args.output_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--nodes', type=int, required=False)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--edge_core', action='store_true')
    group.add_argument('--factory_backbone', action='store_true')
    group.add_argument('--hierarchical_rings_rec', action='store_true')
    group.add_argument('--ring', action='store_true')
    group.add_argument('--line', action='store_true')
    parser.add_argument('--output_file')
    parser.add_argument('--line_graphml', help='set if line topology should be graphml', action='store_true')

    args = parser.parse_args()

    main(args)
