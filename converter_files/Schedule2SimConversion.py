import json
import xml.etree.ElementTree as ET
from collections import OrderedDict
from collections import defaultdict
from itertools import groupby
from os.path import join

import xml.dom.minidom
from lxml import etree
from networkx.readwrite.graphml import write_graphml

from tsnutils.schedules.schedule import schedule_to_xml
from tsnutils.streams.stream import Stream
from utils import get_name_to_id_dict


def write_to_xml(input_xml, output_name):
	with open(output_name, 'w') as output:
		# prettify the output routing
		pretty_xml = xml.dom.minidom.parseString(ET.tostring(input_xml)).toprettyxml()
		output.write(pretty_xml)


def generate_flow_xml(topology, streams, output):
	"""
	:param streams: stream set
	:param output: Output flow.xml file name with .xml extension
	:return:
	"""
	flow = Stream.export_streams_to_xml(streams, topology)
	write_to_xml(flow, output)


def generate_gates_states_from_packet_hist(topology, packet_history_dict):
	# init variables
	try:
		is_switch = topology.vertex_properties['IsSwitch']
	except KeyError:
		is_switch = topology.vertex_properties['is_switch']
	name_to_id_dict = get_name_to_id_dict(topology)
	edge_to_gcl = defaultdict(list)
	# build gate operations for every edge
	for stream_history in packet_history_dict.values():
		sending_node = None
		receiving_node = None
		tx_ts = None
		rx_ts = None
		for idx, event in enumerate(stream_history):
			# skip first packet received event
			if idx == 0:
				continue
			event_ts = event[0]
			event_name = event[1][0]
			event_node = event[1][1]
			if event_name == "tc_packet_transmission_start":
				# TODO implement for cut_through
				sending_node = event_node
				tx_ts = event_ts
			elif event_name == "packet_received":
				receiving_node = event_node
				rx_ts = event_ts
				if is_switch[name_to_id_dict[sending_node]]:
					topology_edge = topology.edge(name_to_id_dict[sending_node], name_to_id_dict[receiving_node])
					edge_to_gcl[topology_edge].append((tx_ts, 1))  # 1 means open gate
					edge_to_gcl[topology_edge].append((rx_ts, 0))  # 0 means close gate
				sending_node = None
				receiving_node = None
				tx_ts = None
				rx_ts = None
	# sort gate operations in GCL of every edge
	edge_to_gcl_shorted = {}
	for edge, gate_control_list in edge_to_gcl.items():
		# sort by timestamp of gate operation, operation itself is tiebraker
		gate_control_list_sorted = sorted(gate_control_list, key=lambda x: (x[0], x[1]))
		# omit unnecessary operations
		gate_control_list_short = [(0, 0)]  # default gate state is all closed at beginning
		idx = 0
		while idx < len(gate_control_list_sorted):
			# don't access last operation
			if idx == len(gate_control_list_sorted) - 1:
				gate_control_list_short.append(gate_control_list_sorted[idx])
				break
			operation_ts_open = gate_control_list_sorted[idx][0]
			operation_ts_close = gate_control_list_sorted[idx + 1][0]
			if operation_ts_open != operation_ts_close:
				gate_control_list_short.append(gate_control_list_sorted[idx])
				idx += 1
			else:
				# oprations can be skipped, if after closing the gate it is directly opened again
				idx += 2
		edge_to_gcl_shorted[edge] = gate_control_list_short
	return edge_to_gcl_shorted


def generate_schedule_xml(topology, packet_history_dict, network_cycle_time_ns, output):
	"""
	Generates GCLs from a given simulation result.
	:param cycle_time takes cycle time from first stream, assumes that every stream and switch execute same network cycle
	:param topology Topology to build schedule for
	:param packet_history_dict result of simulator
	:param output: Output schedule.xml file name with .xml extension
	"""
	# init variables
	v_id = topology.vertex_properties['v_id']
	edge_to_gcl_shorted = generate_gates_states_from_packet_hist(topology, packet_history_dict)

	edge_gcl = defaultdict(list)
	for edge, gcl in edge_to_gcl_shorted.items():
		for idx in range(0, len(gcl)):
			# timestamps are given in GCL, therefore we need to calculate the delta between operations
			operation_ts_1 = gcl[idx][0]
			if idx < len(gcl) - 1:
				operation_ts_2 = gcl[idx + 1][0]
				interval = operation_ts_2 - operation_ts_1
			else:
				# last operation executed until new cycle
				interval = network_cycle_time_ns - operation_ts_1
			operation = gcl[idx][1]
			edge_gcl[edge].append((interval, '{}0000000'.format(operation)))

	# build xml file
	xml_out = schedule_to_xml(topology, network_cycle_time_ns, edge_gcl)

	write_to_xml(xml_out, output)


def generate_topology_graphml(topology, output):
	"""
	Generates .graphml file from given toplogy. Resets all filters.
	:param simulator Simulator for which topology will be saved
	:param output Name of .graphml file with .graphml extension
	"""
	topology.save(output)
	"""
	# reset filters
	topology.set_vertex_filter(None)
	topology.set_edge_filter(None)
	topology.save(output, fmt="graphml")
	"""


def generate_scheduler_requirements(streams, output_path):
	"""
	Converts the output of feasible creator to benchmarking scheduler input format.
	"""
	for stream_id, stream in streams:
		stream.feasible['start'] = stream.state['ed']
		stream.feasible['la'] = stream['la']
		stream.feasible['e2e'] = stream['e2e']
		stream.feasible['path'] = [int(v) for v in stream['path']]

		path_v = stream['path']
		edges = [f"edge_{path_v[idx]}_{path_v[idx + 1]}" for idx in range(len(path_v) - 1)]

		stream.feasible['edges'] = edges
		stream.feasible['paths_e'] = [edges]
		stream.feasible['paths_v'] = [path_v]
		stream.feasible['route_sections_e'] = [edges]
		stream.feasible['vertices'] = path_v
	streams = {"stream_requirements": streams}
	streams_json = "streams_feasible.json"
	with open(join(output_path, streams_json), "w") as f:
		json.dump(streams, f)


def generate_jrs_requirements(streams, output_path):
	# need to add slots, 1 slot=125B
	for stream in streams:
		stream['slots'] = int(stream['size'] / 125)
	NEWLINE_SEP = "\n"
	# copied from scheduling benchmarking code, export_requirements.py
	dat_file = f'''
flows =
{'{'}
{NEWLINE_SEP.join(str(stream['id']) for stream in streams)}
{'}'};

flow_origs =
[
{NEWLINE_SEP.join(str(stream['source']) for stream in streams)}
];

flow_dests =
[
{NEWLINE_SEP.join(str(stream['target']) for stream in streams)}
];

flow_prds =
[
{NEWLINE_SEP.join(str(stream['cycle_time']) for stream in streams)}
];

flow_rsvs =
[
{NEWLINE_SEP.join(str(flow['slots']) for flow in streams)}
];

flow_ddlns =
[
{NEWLINE_SEP.join(str(stream['e2e']) for stream in streams)}
];

'''
	table_file = ""
	for stream in streams:
		table_file += f"{stream['id']} {stream['source']} {stream['target']} {stream['cycle_time']} {stream['slots']} {stream['e2e']}\n"
	with open(join(output_path, "flowparams.dat"), "w") as f:
		f.write(dat_file)
	with open(join(output_path, "flowparams.table"), "w") as f:
		f.write(table_file)


def generate_d3_files(topology, packet_history_dict, streams, output_path):
	d3_gate_states = generate_d3_gate_states(topology, packet_history_dict)
	d3_flow_events = generate_d3_packet_information(topology, packet_history_dict, streams)
	with open(join(output_path, "gate_states.json"), "w") as f:
		json.dump(d3_gate_states, f, indent=2, sort_keys=True)
	with open(join(output_path, "packet_information.json"), "w") as f:
		json.dump(d3_flow_events, f, indent=2, sort_keys=True)


def generate_d3_packet_information(topology, packet_history_dict, streams):
	name_to_id = get_name_to_id_dict(topology)
	# assign ingress ports to every edge
	node_port_map = defaultdict(lambda: 0)
	topology.ep['ingress_port'] = topology.new_edge_property("int")
	for e in topology.edges():
		target = int(e.target())
		topology.ep['ingress_port'][e] = node_port_map[target]
		node_port_map[target] += 1
	packet_information = defaultdict(list)
	pkg_id = 1000 + len(packet_history_dict)
	for stream_id, stream_hist in packet_history_dict.items():
		pkg_id += 1
		# get first event
		event_transmission_start = stream_hist[3]
		time = event_transmission_start[0] * 1e-9
		node = event_transmission_start[1][1]
		# add host events
		packet_information[stream_id].append(
			[round(time, 10), f"{node}", "pktSentFlowId:vector", f"test.{node}.trafGenSchedApp", stream_id])
		packet_information[pkg_id].append(
			[round(time, 10), f"{node}", "pktSentTreeId:vector", f"test.{node}.eth.mac", pkg_id])
		# add switch events
		for event in stream_hist:
			time = event[0] * 1e-9
			node = event[1][1]
			event_type = event[1][0]
			# skip hosts since their events are added seperately
			if not topology.vp['is_switch'][name_to_id[node]]:
				continue
			if event_type == "packet_received":
				sender_node = streams[stream_id].route[streams[stream_id].route.index(node) - 1]
				e = topology.edge(name_to_id[sender_node], name_to_id[node])
				packet_information[pkg_id].append([round(time, 10), f"{node}", "receivedExpressFrame:vector",
												   f"test.{node}.eth[{topology.ep.ingress_port[e]}].mac", pkg_id])
			elif event_type == "tc_packet_queued":
				receiver_node = streams[stream_id].route[streams[stream_id].route.index(node) + 1]
				e = topology.edge(name_to_id[receiver_node], name_to_id[node])
				packet_information[pkg_id].append([round(time, 10), f"{node}", "enqueuePk:vector",
												   f"test.{node}.eth[{topology.ep.egress_port[e]}].queuing.queues[7]",
												   pkg_id])
			elif event_type == "tc_packet_transmission_start":
				receiver_node = streams[stream_id].route[streams[stream_id].route.index(node) + 1]
				e = topology.edge(name_to_id[receiver_node], name_to_id[node])
				packet_information[pkg_id].append([round(time, 10), f"{node}", "startTxExpressFrames:vector",
												   f"test.{node}.eth[{topology.ep.egress_port[e]}].mac", pkg_id])
		event_transmission_end = stream_hist[-1]
		time = event_transmission_end[0] * 1e-9
		node = event_transmission_end[1][1]
		packet_information[pkg_id].append(
			[round(time, 10), f"{node}", "pktRcvdTreeId:vector", f"test.{node}.eth.mac", pkg_id])
		packet_information[stream_id].append(
			[round(time, 10), f"{node}", "pktRcvdFlowId:vector", f"test.{node}.trafGenSchedApp", stream_id])
	return packet_information


def generate_d3_gate_states(topology, packet_history_dict):
	edge_to_gcl_shorted = generate_gates_states_from_packet_hist(topology, packet_history_dict)
	try:
		is_switch = topology.vp['IsSwitch']
	except KeyError:
		is_switch = topology.vp['is_switch']
	# assign egress ports to every edge
	node_port_map = defaultdict(lambda: 0)
	topology.ep['egress_port'] = topology.new_edge_property("int")
	for e in topology.edges():
		source = topology.vp['v_id'][int(e.source())]
		topology.ep['egress_port'][e] = node_port_map[source]
		node_port_map[source] += 1
	d3_gate_states = defaultdict(dict)
	for e in topology.edges():
		# skip edges where source is a host, since hosts do not have gcls
		if not is_switch[e.source()]:
			continue
		source = topology.vp['v_id'][int(e.source())]
		port_name = f"eth[{topology.ep.egress_port[e]}]"
		d3_gate_states[source][port_name] = {}
		for gate_id in range(0, 7):
			# Single entry that closes the gate
			d3_gate_states[source][port_name][f"tGates[{gate_id}]"] = [[0, 0]]
		# if e not in gcl list, no gcl exists -> close all gates
		if e not in edge_to_gcl_shorted:
			d3_gate_states[source][port_name]["tGates[7]"] = [[0, 0]]
		else:
			# time unit of d3 tool is seconds, simulator is nanoseconds -> need to convert
			d3_gate_states[source][port_name]["tGates[7]"] = [
				(max(round(gate_op_tupel[0] * 1e-9, 10), 0.0), gate_op_tupel[1]) for gate_op_tupel in
				edge_to_gcl_shorted[e]]
	return d3_gate_states
