# Overview over verification `.json` files

1. `test_streams_1.json` and `test_topology_1.json` test the following:

- s2 receives frames from s0 and s1. However, it receives frame0 from s0 earlier, therefore frame1 from s1 has to queue
until frame0 is fully received by s3.
- correct trace:
{0: [(0, ('tc_packet_queued', 's0', 0)), (0, ('tc_packet_transmission_start', 's0', 0)), (399, ('header_received', 's2', 0)), (2399, ('packet_processed', 's2', 0)), (2399, ('tc_packet_queued', 's2', 0)), (2399, ('tc_packet_transmission_start', 's2', 0)), (2827, ('header_received', 's3', 0)), (4827, ('packet_processed', 's3', 0)), (11010, ('packet_received', 's2', 0)), (14499, ('packet_received', 's3', 0))]}
{1: [(0, ('tc_packet_queued', 's1', 1)), (0, ('tc_packet_transmission_start', 's1', 1)), (428, ('header_received', 's2', 1)), (2428, ('packet_processed', 's2', 1)), (2428, ('tc_packet_queued', 's2', 1)), (12100, ('packet_received', 's2', 1)), (14499, ('tc_packet_transmission_start', 's2', 1)), (14927, ('header_received', 's3', 1)), (16927, ('packet_processed', 's3', 1)), (26599, ('packet_received', 's3', 1))]}


2. `test_streams_2.json` and `test_topology_2.json` test the following:

- s2 receives frames from s0 and s1. However, it receives frame0 from s0 earlier, therefore frame1 from s1 has to queue
until frame0 is fully received by s3. s3 receives frame0 fully before s2 receives frame1 fully. frame1 is forwarded
beofre it is received fully.
- correct trace:
{0: [(0, ('tc_packet_queued', 's0', 0)), (0, ('tc_packet_transmission_start', 's0', 0)), (399, ('header_received', 's2', 0)), (2399, ('packet_processed', 's2', 0)), (2399, ('tc_packet_queued', 's2', 0)), (2399, ('tc_packet_transmission_start', 's2', 0)), (2827, ('header_received', 's3', 0)), (4827, ('packet_processed', 's3', 0)), (7373, ('packet_received', 's2', 0)), (10499, ('packet_received', 's3', 0))]}
{1: [(0, ('tc_packet_queued', 's1', 1)), (0, ('tc_packet_transmission_start', 's1', 1)), (428, ('header_received', 's2', 1)), (2428, ('packet_processed', 's2', 1)), (2428, ('tc_packet_queued', 's2', 1)), (10499, ('tc_packet_transmission_start', 's2', 1)), (10927, ('header_received', 's3', 1)), (12100, ('packet_received', 's2', 1)), (12927, ('packet_processed', 's3', 1)), (22599, ('packet_received', 's3', 1))]}
