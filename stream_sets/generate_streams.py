#!/usr/bin/python3
import json
import random
from math import ceil
from collections import defaultdict
import graph_tool.all as gt
import networkx as nx

from tsnutils.topology.topology import parse_topology
from utils import read_topology
from utils import get_name_to_id_dict


def merge_streams_routes(streams_path, routes_path):
    """
    Takes streams and outes json as inputs and maps routes to corresponding streams. Neccessary, because in
    scheduling-benchmarking routes and streams are seperate. Saves new streams at given path.
    :param streams_path: Path to streams.json
    :param routes_path: Path to routes.json
    """
    with open(streams_path, 'r') as f:
        try:
            streams = json.load(f)['stream_requirements']
        except KeyError:
            streams = json.load(f)
    with open(routes_path, 'r') as f:
        routes = json.load(f)['routes']
    streams_with_routes = []
    # merging routes and streams
    for stream in streams:
        # convert names to strings
        stream['source'] = str(stream['source'])
        stream['target'] = str(stream['target'])
        stream_route = routes[str(stream['id'])]['vertices']
        stream_route = [str(v) for v in stream_route]
        stream['path'] = stream_route
        streams_with_routes.append(stream)
    with open(streams_path, 'w') as f:
        json.dump(streams_with_routes, f)


def convert_streams(streams_path, cycle_time, safety_factor):
    """
    This method executes minor conversions to make stream sets from the benchmarking tool compatible with this scheduler.
    :param streams_path: Path to streams
    :param cycle_time: cycle time in [mu s]
    """
    with open(streams_path, 'r') as f:
        streams = json.load(f)
    # rename start
    for stream in streams:
        stream['ed'] = stream.pop('start')
        # unit in benchmarking stream format is slots, 1 slot corresponds to 1 mu s
        stream['ed'] *= 1000
        stream['e2e'] *= 1000
        if not 'la' in stream:
            stream['la'] = round((stream['ed'] + stream['e2e']) * safety_factor)
            # stream['la'] = stream['e2e'] * safety_factor
        else:
            stream['la'] *= 1000
        stream['ed'] = 0
        stream['cycle_time'] = cycle_time * 1000
    with open(streams_path, 'w') as f:
        json.dump(streams, f)


def get_conflict_streams_naive(streams, late_streams_idx):
    """
    Returns mapping for every late stream and the corresponding conflicting streams. Naive approach.
    """
    mapping = defaultdict(list)
    if not late_streams_idx:
        return mapping
    late_streams_idx = set(late_streams_idx)
    for late_idx in late_streams_idx:
        conflict_edges = set(streams[late_idx].route)
        for idx, stream in enumerate(streams):
            if idx in late_streams_idx:
                continue
            edges = set(stream.route)
            if conflict_edges & edges:
                mapping[late_idx].append(idx)
    return mapping


def get_conflict_streams_link_time(streams, late_streams_idx, packet_history_dict):
    class LinkOccupation:
        def __init__(self, source, target, queue_time, rx_time):
            self.source = source
            self.target = target
            self.rx_time = rx_time
            self.queue_time = queue_time

        def __str__(self):
            return "src: {} -> trg: {} ~ queue time: {} -> rx time: {}".format(self.source, self.target,
                                                                               self.queue_time, self.rx_time)

        def overlap(self, other) -> bool:
            """
            Checks if this link occupation overlaps with other link occupation.
            """
            if self.source != other.source or self.target != other.target:
                return False
            # if streams queue at same time, they overlap
            if self.queue_time <= other.queue_time < self.rx_time:
                return True
            elif other.queue_time <= self.queue_time < other.rx_time:
                return True
            else:
                return False

        def get_overlap_time(self, other) -> [int, None]:
            """
            Calculates overlap time of link occupations.
            :return: if they don't overlap, returns None. Else, return positive integer, if self is queued after other,
            return negative integer if self is queued before other.
            """
            if not self.overlap(other):
                return None
            else:
                return self.queue_time - other.queue_time

    mapping = defaultdict(list)
    times = defaultdict(list)

    # build link occupation for every stream from packet history
    streams_link_occup_dict = {}
    for stream_id, stream_hist in packet_history_dict.items():
        streams_link_occup = []
        queue_event = None
        for event in stream_hist:
            # TODO implement cut-through
            if event[1][0] == 'tc_packet_queued':
                queue_event = event
            elif event[1][0] == 'packet_received':
                link_occup = LinkOccupation(queue_event[1][1], event[1][1], queue_event[0], event[0])
                streams_link_occup.append(link_occup)
                queue_event = None
        streams_link_occup_dict[int(stream_id)] = streams_link_occup

    # check for every late stream if link occupations overlap with any other stream
    for late_idx in late_streams_idx:
        for to_check_index in streams_link_occup_dict.keys():
            # need to check every link occupation against every other
            if late_idx == to_check_index:
                continue
            overlap_flag = False
            for late_stream_link_occup in streams_link_occup_dict[late_idx]:
                for streams_link_occup in streams_link_occup_dict[to_check_index]:
                    overlap_time = late_stream_link_occup.get_overlap_time(streams_link_occup)
                    if overlap_time is not None:
                        mapping[late_idx].append(to_check_index)
                        # positive number means late stream is queued after other stream
                        # negative means the opposite
                        # 0 means they queue at the same time
                        times[late_idx].append((to_check_index, overlap_time))
                        # do not need to check other link occupations if it overlapped
                        overlap_flag = True
                        break
                if overlap_flag:
                    break

    return mapping, times


def build_conflict_graph(mapping, streams):
    """
    Builds a conflict graph for a given late stream to conflicting streams mapping.
    """
    # create graph
    conflict_graph = gt.Graph()
    max_v = max([stream_id for stream_id in streams]) + 1
    conflict_graph.add_vertex(max_v)
    """
    This is buggy, last vertex does not get set to correct value!
    conflict_graph.vertex_properties['not_removed'] = conflict_graph.new_vertex_property('bool', val=True)
    """
    tmp1 = conflict_graph.new_vertex_property('bool')
    conflict_graph.vertex_properties['not_removed'] = tmp1
    for v in conflict_graph.vertices():
        conflict_graph.vertex_properties['not_removed'][v] = True
    # create edges, from late stream to conflicting stream
    for late_idx in mapping.keys():
        for idx in mapping[late_idx]:
            conflict_graph.add_edge(streams[late_idx].id, streams[idx].id)
    return conflict_graph


def sum_remaining_time(streams, packet_history_dict, late_streams_idx):
    """
    Sums the remaining time to latest arrival for every stream.
    :return:
    """
    sum = 0
    for idx in late_streams_idx:
        sum += streams[idx].state['la'] - packet_history_dict[idx][-1][0]
    return sum


if __name__ == '__main__':
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--ns', type=int, required=True, help='Number of streams')
    parser.add_argument('--cycle', type=int, required=True, help='Cycle time of hosts in ns')
    parser.add_argument('--topology', type=str, required=True, help='Path to topology file')
    parser.add_argument('--output', required=True, help='Output file')

    args = parser.parse_args()

    generate_streams(args.ns, args.cycle, args.topology, args.output)

    # main('test_topology.json', 100, 'test_s.xml', True)
    """
    merge_streams_routes("streams_benchmarking.json", "routes_benchmarking.json")
    convert_streams("streams_benchmarking.json", 1000)
