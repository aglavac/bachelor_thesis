from create_queueing_feasible import create_feasible_v1
import random
import os
import argparse
import json
from tracing_based_scheduling_converter import convert_files
from utils import read_topology
from utils import write_gt_to_json


def generate_stream_set(no, topology_path, nstrms, cycles, no_late_streams):
	"""
	Generates multiple stream sets with routes for a given topology.
	:param no: Number of stream sets to generate.
	:return:
	"""
	# need to save topology, since converter script renames attributes. Need original topology for feasible gen to work.
	topology = read_topology(topology_path)
	working_dir = "/home/alex/Documents/bachelorthesis/code/"
	os.chdir(working_dir)
	dir = "/home/alex/Documents/bachelorthesis/code/stream_sets/"
	stream_filename = "stream_set_{}.json"
	for i in range(no):
		# init randomness
		random.seed(i)
		streams_path = stream_filename.format(i)
		input_filename = os.path.join(dir, streams_path)
		no_streams = random.choice(nstrms)
		cycle = random.choice(cycles)
		ls = random.choice(no_late_streams)
		create_feasible_v1(os.path.join(dir, topology_path), False, cycle, no_streams, [ls], 1, "dbg/", seed_state=random.getstate())
		# get correct input for scheduler
		topo_param = os.path.join(dir, topology_path)
		streams_param = os.path.join(working_dir, "dbg/streams_feasible.json")
		routes_param = os.path.join(working_dir, "dbg/routes_feasible.json")
		args = argparse.Namespace(topology_path=topo_param, streams_path=streams_param, routes_path=routes_param, cycle_time=int(cycle / 1000))
		convert_files(args)
		os.rename(streams_param, input_filename)
		write_gt_to_json(topology, topo_param)


if __name__ == "__main__":
	generate_stream_set(500, "topology.json", [100], [1000000], [1, 2, 3, 5, 7, 10, 13, 17])

