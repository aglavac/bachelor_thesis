class NoSolutionException(Exception):
	pass


class SchedulingMethodNotFound(Exception):
	pass