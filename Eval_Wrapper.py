#!./venv/bin/python3
from SimulationWrapper import TracingScheduling
from MCTS import MCTS
import glob
from re import search


def eval(eval_dir):
	# get result files
	directory = 'stream_sets/'
	# Get a list of all result files in all given directories
	result_pattern = "stream_set_*.json"
	stream_set_list = glob.glob(directory + result_pattern, recursive=True)
	topology_path = "stream_sets/topology.graphml"

	# setting eval directory
	if eval_dir == "link_time_ls":
		eval_output_path = 'link_time_ls_results/{}_results.json'
	elif eval_dir == "link_time_rem_time":
		eval_output_path = 'link_time_rem_time_results/{}_results.json'
	elif eval_dir == "naive":
		eval_output_path = 'naive_results/{}_results.json'
	elif eval_dir == "link_time_mcts":
		eval_output_path = 'link_time_mcts_results/{}_results.json'
	else:
		print("Unknown eval directory! Exiting...")
		return

	for stream_set in stream_set_list:
		stream_set_no = int(search("stream_set_(\d*).json", stream_set).group(1))
		if eval_dir in ["link_time_ls", "link_time_rem_time", "naive"]:
			s = TracingScheduling(eval_output_path.format(stream_set_no), topology_path, stream_set, 120, False,
				 rescheduling_method=eval_dir, log=False)
			s.run_scheduling()
		elif eval_dir in ["link_time_mcts"]:
			mcts = MCTS(eval_output_path.format(stream_set_no), topology_path, stream_set, 120, False, "converter_files")
			mcts.export_solution()


if __name__ == "__main__":
	eval("link_time_ls")
