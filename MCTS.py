#!./venv/bin/python3
import json
import math
import pickle
import random
from collections import namedtuple
from time import perf_counter
from os import path
import numpy as np
import networkx as nx

from SimulationWrapper import get_late_streams
from simulator.PacketSimulator import PacketSimulator
from stream_sets.generate_streams import get_conflict_streams_link_time
from converter_files.Schedule2SimConversion import generate_topology_graphml
from converter_files.Schedule2SimConversion import generate_flow_xml
from converter_files.Schedule2SimConversion import generate_schedule_xml
from SimulationWrapper import print_output
from SimulationWrapper import TracingScheduling
from streams.stream import parse_streams
from tsnutils.topology.topology import parse_topology
from utils import read_topology

Shift = namedtuple('Shift', ['stream_to_shift', 'shift'])


class MCTS:
	def __init__(self, eval_output_path, topology_path, streams_path, runtime, cut_through, xml_directory, log=False):
		# initialization
		self.log = log
		self.xml_dir = xml_directory
		self.eval_output_path = eval_output_path
		self.sim_output = {}
		self.solution_packet_history_dict = None
		topology = parse_topology(topology_path)
		streams = parse_streams(topology, streams_path)
		self.stream_set = StreamSet(streams, topology, cut_through)
		t_start = perf_counter()
		t_end = t_start + runtime  # runtime [s]
		# save initial state
		self.sim_output[t_start] = [list(self.stream_set.root.late_streams), 0]
		while perf_counter() < t_end:
		# while True:
			# find expanded node from tree policy
			best_node = self.stream_set.root.tree_policy(self.stream_set)
			# pass stream set according to best_child stream set
			delta = best_node.default_policy(self.stream_set)
			best_node.backup(delta)

			# log output
			t_schedule = perf_counter() - t_start
			self.best_child = self.stream_set.root.find_best_node()
			self.sim_output[t_schedule] = [list(self.best_child.late_streams), t_schedule]
			if not self.best_child.late_streams:
				break

	def prepare_solution(self):
		self.stream_set.simulate_shifts_from_node(self.best_child)
		# do actual simulation
		simulator = PacketSimulator(self.stream_set.topology, self.stream_set.is_cut_through)
		simulator.add_packets(self.stream_set.streams)
		simulator.execute_simulation()
		# get sim results
		with open("packet_history_dict.bin", 'rb') as pickle_file:
			self.solution_packet_history_dict = pickle.load(pickle_file)

	def export_solution(self):
		self.prepare_solution()

		# print results
		print_output(self.sim_output, self.eval_output_path, self.log, self.solution_packet_history_dict)
		self.render_tree()
		# generate converter files
		toplogy_graphml = "topology.graphml"
		flow_xml = "flows.xml"
		schedule_xml = "schedule.xml"
		generate_topology_graphml(self.stream_set.topology, path.join(self.xml_dir, toplogy_graphml))
		generate_flow_xml(self.topology, self.stream_set.streams, path.join(self.xml_dir, flow_xml))
		generate_schedule_xml(self.stream_set.topology, self.solution_packet_history_dict,
							  self.stream_set.streams[0]['cycle_time'], path.join(self.xml_dir, schedule_xml))

	def render_tree(self):
		root = self.stream_set.root
		node_id = 0
		tree = nx.Graph()
		tree.add_node(node_id)
		stack = []
		stack.insert(0, (root, node_id))
		node_id += 1
		while stack:
			n, n_id = stack.pop()
			for c in n.children:
				stack.insert(-1, (c, node_id))
				tree.add_node(node_id)
				tree.add_edge(n_id, node_id)
				node_id += 1
		pgv_tree = nx.nx_agraph.to_agraph(tree)
		# pgv_tree.node_attr['style']='filled'
		# pgv_tree.node_attr['color']='grey'
		pgv_tree.node_attr['shape']='circle'
		# pgv_tree.node_attr['fixedsize']='true'
		pgv_tree.node_attr['fontsize']='25'
		pgv_tree.node_attr['fontcolor']='#000000'
		for n in pgv_tree.nodes():
			# n.attr['label'] = n.attr['rem_time']
			n.attr['label'] = ""
		pgv_tree.layout(prog='dot')
		print(pgv_tree.string())
		pgv_tree.draw('test.pdf')


class StreamSet:
	"""
	Class representing a stream set. Provides methods to simulate stream shifts.
	"""

	def __init__(self, streams, topology, is_cut_trough):
		self.streams = streams
		self.topology = topology
		self.is_cut_through = is_cut_trough
		self.shift_count = 0
		self.root = StreamNode(self, None, None)
		self.shift_stack = []

	def simulate_shift(self, shift: Shift):
		if shift.shift < 0:
			raise ValueError("Negative shifts are not allowed!")
		if shift is None:
			return
		self.shift_count += 1
		self.push(shift)

	def simulate_shifts_from_node(self, stream_node):
		for shift in stream_node.shift_history():
			self.simulate_shift(shift)

	def reset_simulated_shifts(self):
		for _ in range(self.shift_count):
			self.pop()
		self.shift_count = 0

	def push(self, shift: Shift):
		self.shift_stack.append(shift)
		self.streams[shift.stream_to_shift]["ed"] += shift.shift

	def pop(self):
		popped_shift: Shift = self.shift_stack.pop()
		self.streams[popped_shift.stream_to_shift]["ed"] -= popped_shift.shift

	def get_next_shift(self):
		"""
		Generates the next shift based on the current stream set in self.streams.
		"""
		pass


class StreamNode:
	def __init__(self, stream_set: StreamSet, parent, shift: [Shift, None]):
		# MCTS variables
		self.shift = shift
		self.visits = 0
		self.sum_of_rewards = 0
		self.parent = parent
		self.children = []
		# Scheduling variables
		stream_set.simulate_shifts_from_node(self)

		# do actual simulation
		simulator = PacketSimulator(stream_set.topology, stream_set.is_cut_through)
		simulator.add_packets(stream_set.streams)
		simulator.execute_simulation()

		# get sim results
		with open("packet_history_dict.bin", 'rb') as pickle_file:
			packet_history_dict = pickle.load(pickle_file)
		self.late_streams, _, _ = get_late_streams(stream_set.streams, packet_history_dict)
		mapping, times = get_conflict_streams_link_time(stream_set.streams, self.late_streams, packet_history_dict)
		# get next shifts
		# self.untried_shifts = [Shift(shift[0], shift[1]) for conflict_streams in self.times.values() for shift in
		# 					   conflict_streams if shift[1] >= 0]
		self.untried_shifts = []
		for conflict_streams in times.values():
			for shift in conflict_streams:
				if shift[1] > 0:
					self.untried_shifts.append(Shift(shift[0], shift[1]))
				elif shift[1] == 0:
					self.untried_shifts.append(Shift(shift[0], 1000))
		stream_set.reset_simulated_shifts()

	def add_child(self, child):
		self.children.append(child)

	def shift_history(self):
		"""
		Generates the history of stream shifts.
		"""
		if self.parent is not None:
			for action in self.parent.shift_history():
				yield action
			if self.shift is not None:
				yield self.shift

	def has_next_shift(self):
		return len(self.untried_shifts) > 0

	def best_child(self, beta):
		"""
		Returns the best child according to upper confidence boundary.
		:param beta: Beta to balance exploration and exploitation.
		:return:
		"""
		if not self.children:
			return self
		else:
			ucb1_values = []
			for child in self.children:
				exploitation = child.sum_of_rewards / child.visits
				exploration = math.sqrt(2 * np.log(self.visits) / child.visits)
				ucb1_value = exploitation + beta * exploration
				ucb1_values.append(ucb1_value)
			max_index = np.argmax(ucb1_values)
			return self.children[max_index]  # get best child according to ucb value

	def backup(self, reward):
		"""
		Does backup by updating visits and reward of each node
		:param reward:
		:return:
		"""
		v = self
		while v is not None:
			v.visits += 1
			v.sum_of_rewards += reward
			v = v.parent

	def tree_policy(self, stream_set: StreamSet):
		v = self
		while v.has_next_shift() or v.children:
			if v.has_next_shift():
				return v.expand(stream_set)
			else:
				v = v.best_child(0.8)
		return v

	def find_best_node(self):
		v = self
		while v.children:
			v = v.best_child(0)
		return v

	def expand(self, stream_set: StreamSet):
		if not self.untried_shifts:
			return None
		next_shift = random.choice(self.untried_shifts)
		self.untried_shifts.remove(next_shift)
		child = StreamNode(stream_set, self, next_shift)
		self.add_child(child)
		return child

	def default_policy(self, stream_set: StreamSet):
		v = self
		stream_set.simulate_shifts_from_node(v)
		# reward = self.calculate_reward(stream_set)
		default_policy_scheduling = TracingScheduling("", stream_set.topology, stream_set.streams, 0.5, stream_set.is_cut_through, rescheduling_method="link_time_ls", output=False)
		default_policy_scheduling.run_scheduling()
		_, default_policy_late_streams = default_policy_scheduling.export_result_stream_set()
		reward = self.calculate_reward(default_policy_late_streams)
		stream_set.reset_simulated_shifts()
		return reward

	def calculate_reward(self, late_streams):
		return -1 * (10 * len(late_streams)) ** 2


if __name__ == "__main__":
	mcts = MCTS("test_result.json", "dbg/topology.json", "dbg/streams_feasible.json", 300, False,
				"dbg/")
	# mcts = MCTS("dbg/", "stream_sets/topology.graphml", "stream_sets/stream_set_2.json", 60,
	# 			False, "converter_files", log=True)
	mcts.export_solution()
