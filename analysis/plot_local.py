import glob
import json
from collections import defaultdict

import graph_tool.all as gt
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

from stream_sets.generate_streams import build_conflict_graph
from stream_sets.generate_streams import get_conflict_streams_link_time
from stream_sets.generate_streams import get_conflict_streams_naive


def calculate_bins(number_observations, max_min):
	"""
	Calcs bins for histograms. Source: https://datatofish.com/plot-histogram-python/
	:param number_observations:
	:param max_min:
	:return:
	"""
	number_bins = int(np.ceil(number_observations ** 0.5))
	width_bins = max_min / number_bins
	bins = [width_bins * i for i in range(number_bins)]
	return number_bins, width_bins, bins


def get_result_file_list(directory, pattern):
	return glob.glob(directory + pattern, recursive=True)


def mean_confidence_interval(data, confidence=0.9):
	"""
	Calculate the confidence interval
	:param data: Data to calculate confidence interval from
	:param confidence: Confidence level
	:return: mean, symmetric confidence interval
	"""
	a = 1.0 * np.array(data)
	n = len(a)
	m, se = np.mean(a), np.std(a)
	h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
	return m, h


def read_result(result_file):
	"""
	Returns the json result file.
	"""
	with open(result_file, "r") as f:
		result = json.load(f)
	return result


def analyze_conflicts(approach):
	"""
	Visualizes stream conflicts from results files for every iteration. Can use multiple methods to find conflicts.
	:param approach: Approach to find conflicts
	"""
	directory = '../naive_results/'
	pattern = "*results.json"
	# get list of result files that match pattern
	result_file_list = get_result_file_list(directory, pattern)
	for result_file in result_file_list:
		with open(result_file, "r") as f:
			result = json.load(f)
		# visualize conflicts for every iteration
		for iteration in result.keys():
			late_streams_idx = result[iteration][0]
			print("Late Streams:" + str(late_streams_idx))
			streams = result[iteration][1]
			packet_history_dict = result[iteration][2]
			# use given approach
			if approach == 'naive':
				mapping = get_conflict_streams_naive(streams, late_streams_idx)
			elif approach == 'link_time':
				mapping, _ = get_conflict_streams_link_time(streams, late_streams_idx, packet_history_dict)
			conflict_graph = build_conflict_graph(mapping, streams)
			pos = gt.arf_layout(conflict_graph, d=0.5, a=5)
			gt.graph_draw(conflict_graph, pos=pos)


def get_results_list(results_dir, results_pattern):
	"""
	Gets all results files matching a specific pattern in a directory and returns a list of results.
	:return:
	"""
	# get result files
	results_files_list = get_result_file_list(results_dir, results_pattern)
	return [read_result(results_file) for results_file in results_files_list]


def aggregate_results(results_list, no_bins=0):
	"""
	Aggregates the results into a given number of bins
	"""
	# extract relevant data
	# list of tupels: (timestamp, # of late streams)
	# results_list = [(it_result[1], len(it_result[0])) for result in results_list for iteration, it_result in result.items()]
	relevant_results_list = []
	for result in results_list:
		time = []
		ls = []
		for iteration, it_result in result.items():
			# dict also has non float keys, these are irrelevant for this analysis
			try:
				float(iteration)
			except ValueError:
				break
			time.append(it_result[1])
			ls.append(len(it_result[0]))
		relevant_results_list.append((time, ls))
	# init
	aggregated = defaultdict(list)
	# get maximum number of late streams
	max_ls = max([result[1][0] for result in relevant_results_list])
	if no_bins == 0:
		no_bins = max_ls
	# establish bins
	bin_size = int(max_ls / no_bins)
	bins = [bin_size * i for i in range(1, no_bins)]
	bins.append(max_ls)
	bins_str = [f"{bins[i - 1] + 1}-{bins[i]} l.str." for i in range(1, len(bins))]
	bins_str.insert(0, f"1-{bins[0]} l.str.")

	# start binning
	for result in relevant_results_list:
		# calc initial number of late streams
		init_number_late_streams = result[1][0]
		# skip results with no initial late streams
		if not init_number_late_streams:
			continue
		# find correct bin
		for idx, bin in enumerate(bins):
			if int(init_number_late_streams / (bin + 0.5)) == 0:
				aggregated[bins[idx]].append(result)
				break
	return bins, bins_str, aggregated


def plot_solved_stream_sets_binned(results_list, max_time, no_bins=0):
	"""
	Plots the ratio of solved stream set with a given bin size.
	Bin size refers to the number of late streams for which results are aggregated.
	:return:
	"""
	# bins, bins_str, aggregated_results = get_aggregated_resuls(results_dir, results_pattern, no_bins=no_bins)
	bins, bins_str, aggregated_results = aggregate_results(results_list, no_bins=no_bins)
	fig, ax = plt.subplots(nrows=1, ncols=1)
	for init_ls in sorted(aggregated_results.keys()):
		x_y_list = aggregated_results[init_ls]
		scale = range(0, max_time)
		x = []
		y = []
		no_stream_sets = len(x_y_list)
		for cumu_time in scale:
			x.append(cumu_time)
			y_solved_counter = 0
			for result_time, result_no_ls in x_y_list:
				# search for first occurence where simulation time is larger than cumu time
				for idx in range(len(result_time)):
					if result_time[idx] > cumu_time:
						if idx != 0:
							idx -= 1
						break
					elif result_no_ls[idx] == 0:
						break
				is_solved = result_no_ls[idx] == 0
				if is_solved:
					y_solved_counter += 1
			y.append(y_solved_counter / no_stream_sets)
		ax.plot(x, y, label=bins_str[bins.index(init_ls)])
		ax.set_xlabel('Scheduling Runtime [s]')
		ax.set_ylabel('Ratio of Solved Stream Sets')
	plt.legend()
	plt.show()


def local_plot_solved_stream_sets_binned(results_dir, results_pattern, max_time, no_bins=0):
	"""
	Plots the ratio of solved stream sets from local result files.
	"""
	results_list = get_results_list(results_dir, results_pattern)
	plot_solved_stream_sets_binned(results_list, max_time, no_bins=no_bins)


def get_flowspans(results_list):
	"""
	Calculates the flowspans from a results list.
	"""
	flowspans = []
	# calculate flowspans from packet history dicts
	for result in results_list:
		packet_history_dict = result['packet_history_dict']
		# get earliest deprature time
		# stream_hist[2] = transmission start event, stream_hist[0] = timestamp
		flowspan_start = min([stream_hist[2] for stream_hist in packet_history_dict.values()], key=lambda x: x[0])
		flowspan_finish = max([stream_hist[-1] for stream_hist in packet_history_dict.values()], key=lambda x: x[0])
		flowspans.append(flowspan_finish[0] - flowspan_start[0])
	return flowspans


def plot_flowspans(results_list, cycle_time):
	"""
	Visualizes the flowspan of the solutions. Plots a histogram showcasing the ratio of flowspan to cycle_time.
	"""
	flowspans = get_flowspans(results_list)
	ratios_flowspan_cycle = [flowspan / cycle_time for flowspan in flowspans]
	# we can rate the ability to minimize flowspan by calculating skew if histograms for each scheduler
	plt.hist(ratios_flowspan_cycle, bins=[0.1 * i for i in range(11)])
	plt.show()


def local_plot_flowspans(results_dir, results_pattern, cycle_time):
	"""
	Uses local files to plot flowspans.
	"""
	results_list = get_results_list(results_dir, results_pattern)
	plot_flowspans(results_list, cycle_time)


def print_packet_history_dict(summary_json_path):
	with open(summary_json_path, "r") as f:
		summary_json = json.load(f)
	# build path
	print("Paths:")
	for stream_id, stream_hist in summary_json["packet_history_dict_sol"].items():
		path = []
		path_set = set()
		for event in stream_hist:
			if event[1][1] not in path_set:
				path.append(event[1][1])
			path_set.add(event[1][1])
		print(f"Stream: {stream_id} ~ {path}")
	print("Histories:")
	for stream_id, stream_hist in summary_json["packet_history_dict_sol"].items():
		print(f"Stream: {stream_id} ~ {stream_hist}")
