import pymysql
import configparser


class Eval_DB_Connection:
	"""
	Creates a connection to the database on remote server to get eval data.
	"""
	def __init__(self, ini_filename):
		self.db_env = None
		self.read_db_ini(ini_filename)
		self.connection = None  # type: pymysql.Connection
		self.create_db_connection()

	def read_db_ini(self, ini_filename):
		config = configparser.ConfigParser()
		config.read(ini_filename)
		self.db_env = config['DATABASE']

	def create_db_connection(self):
		self.connection = pymysql.connect(
			host=self.db_env['MYSQL_HOST'],
			port=int(self.db_env['MYSQL_PORT']),
			user=self.db_env['MYSQL_USER'],
			password=self.db_env['MYSQL_PASSWORD'],
			db=self.db_env['MYSQL_DATABASE'],
			charset='utf8mb4',
			cursorclass=pymysql.cursors.DictCursor,
			autocommit=True
		)

	def connection(self):
		return self.connection

	def get_from_db(self, table, id_to_retrieve):
		with self.connection.cursor() as cursor:
			sql = f"SELECT * FROM `{table}` WHERE `id`=%s"
			cursor.execute(sql, (id_to_retrieve))
			task_entry = cursor.fetchone()
		if not task_entry:
			raise ValueError("Requested task does not exist in DB.")
		return task_entry

	def get_query_from_table(self, sql_query, query_params: tuple):
		with self.connection.cursor() as cursor:
			cursor.execute(sql_query, query_params)
			task_entry = cursor.fetchall()
		if not task_entry:
			raise ValueError("Requested query was not successfull.")
		return task_entry

	def close_connection(self):
		self.connection.close()


if __name__ == '__main__':
	db_con = Eval_DB_Connection("db-env.ini")
	entry = db_con.get_from_db("feasibles", 60)
	print(entry['feasible_type'])
	db_con.close_connection()
