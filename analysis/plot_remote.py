#!/usr/bin/env python3

import argparse
import json
import pickle
from collections import OrderedDict
from math import isnan

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd
from Eval_DB_Connection import Eval_DB_Connection

# from analysis.Eval_DB_Connection import Eval_DB_Connection

plt.rcParams['text.usetex'] = True
plt.rcParams.update({'font.size': 19})

scheduler_table_mapping = {
	"tsn_smt": "tsn_smt1_tasks",
	"jssp_shortest_path": "jssp_tasks",
	"jssp_precalculated": "jssp_tasks",
	"tracing_naive": "tracing_tasks",
	"tracing_link_time_ls": "tracing_tasks",
	"tracing_link_time_rem_time": "tracing_tasks",
	"tracing_link_time_mcts": "tracing_tasks"
}

scheduler_name_mapping = {
	"tsn_smt": "TSN SMT",
	"jssp_shortest_path": "JSSP:\nShortest Path Routing",
	"jssp_precalculated": "JSSP:\nPrecalculated Routes",
	"tracing_naive": "TBS:\nnaive",
	"tracing_link_time_ls": "TBS:\nLate Streams",
	"tracing_link_time_rem_time": "TBS:\nRemaining Time",
	"tracing_link_time_mcts": "TBS:\nMCTS"
}

scheduler_marker_mapping = {
	"tsn_smt": "s",
	"jssp_shortest_path": "x",
	"jssp_precalculated": "x",
	"tracing_naive": "^",
	"tracing_link_time_ls": "^",
	"tracing_link_time_rem_time": "^",
	"tracing_link_time_mcts": "^"
}


def escape_latex(s: str):
	s = s.replace("_", "\_")
	return s


def get_scheduler_table(scheduler):
	return scheduler_table_mapping[scheduler]


def get_scheduler_method(scheduler):
	"""
	Returns the method (tracing)/routing (jssp) option for the corresponding scheduler
	"""
	scheduler_method_mapping = {
		"tsn_smt": "",
		"jssp_shortest_path": "SHORTEST_PATH",
		"jssp_precalculated": "PRECALCULATED_PATH",
		"tracing_naive": "naive",
		"tracing_link_time_ls": "link_time_ls",
		"tracing_link_time_rem_time": "link_time_rem_time",
		"tracing_link_time_mcts": "link_time_mcts"
	}
	return scheduler_method_mapping[scheduler]


def get_simgen_type(scheduler):
	"""
	Returns the value of the column type in the sim_gen_tasks table for the corresponding scheduler.
	"""
	scheduler_type_mapping = {
		"tsn_smt": "tsn_smt1",
		"jssp_shortest_path": "jssp",
		"jssp_precalculated": "jssp",
		"tracing_naive": "tracing",
		"tracing_link_time_ls": "tracing",
		"tracing_link_time_rem_time": "tracing",
		"tracing_link_time_mcts": "tracing"
	}
	return scheduler_type_mapping[scheduler]


def get_feasible_table(feas_type):
	return feas_type + "_tasks"


def establish_bins(max_ls, no_bins):
	"""
	Generates bins and names for the bins to aggregate solved stream sets by the initial number of late streams.
	:return:
	"""
	bin_size = int(max_ls / no_bins)
	bins = [bin_size * i for i in range(1, no_bins)]
	bins.append(max_ls)
	bins_str = [f"{bins[i - 1] + 1}-{bins[i]} l.str." for i in range(1, len(bins))]
	bins_str.insert(0, f"1-{bins[0]} l.str.")
	return bins, bins_str


def show_finished_plot():
	# plt.legend(loc='upper right', prop={'size': 13}, fancybox=True, shadow=True)  # bbox_to_anchor=(1, 1)
	plt.show()


def plot_solved_stream_sets_all(df_filter, no_bins=0):
	fig, axs = plt.subplots(3, 3)
	axs_flat = axs.flatten()
	for idx, scheduler in enumerate(scheduler_table_mapping):
		axs_flat[idx] = plot_solved_stream_sets(scheduler, df_filter, use_pickle=True, no_bins=no_bins,
												use_this_ax=axs_flat[idx], use_this_fig=fig, legend=False,
												prettify=False)
		axis_font = {'size': '12'}
		axs_flat[idx].set_ylabel(escape_latex("Ratio Solved"), usetex=True, **axis_font)
		axs_flat[idx].set_ylim(bottom=-0.05, top=1.05)
		axis_font = {'size': '10'}
		axs_flat[idx].set_xlabel(escape_latex("Time [s]"), usetex=True, **axis_font)
		axs_flat[idx].set_title(escape_latex(scheduler_name_mapping[scheduler]), usetex=True, **axis_font)
		axs_flat[idx].tick_params(axis='both', labelsize=8)
	handles, labels = axs_flat[-3].get_legend_handles_labels()
	title = "Ratio of Solved Stream Sets"
	if not df_filter:
		title += " - Filter: None"
	for f, v in df_filter.items():
		title += f"- {f}: {v} "
	axis_font = {'size': '18'}
	fig.suptitle(escape_latex(title), usetex=True, **axis_font)
	fig.tight_layout()
	fig.subplots_adjust(wspace=0.24, hspace=0.41, left=0.05, bottom=0.05, top=0.90)
	fig.legend(handles, labels, loc='lower left', ncol=1, prop={'size': 15}, bbox_to_anchor=(0.35, 0.025))
	# fig.legend(handles, labels, ncol=2)
	fig.delaxes(axs_flat[-1])
	fig.delaxes(axs_flat[-2])
	# fig.delaxes(axs_flat[0])
	# fig.delaxes(axs_flat[1])
	# fig.delaxes(axs_flat[2])
	# fig.delaxes(axs_flat[3])
	# fig.delaxes(axs_flat[4])
	# fig.delaxes(axs_flat[5])
	# fig.delaxes(axs_flat[6])
	# fig.delaxes(axs_flat[7])
	# fig.delaxes(axs_flat[8])
	# show_finished_plot()
	plt.show()


def plot_solved_stream_sets(method, df_filter, use_pickle=True, no_bins=5, use_this_ax=None, use_this_fig=None,
							legend=True, prettify=True):
	"""
	Plots the ratio of solved stream sets for a given scheduler.
	:param no_bins: Bins by inital number of late streams.
	"""
	if not df_filter:
		pickle_filename = f"{method}.bin"
	else:
		pickle_filename = f"{method}"
		for f, v in df_filter.items():
			pickle_filename += f"_{f}_{v}"
		pickle_filename += ".bin"
	if use_pickle:
		try:
			with open(pickle_filename, "rb") as f:
				times, ratio_solved, bins_str = pickle.load(f)
		except FileNotFoundError:
			use_pickle = False
	if not use_this_ax:
		fig, ax = plt.subplots(nrows=1, ncols=1)
	else:
		ax = use_this_ax
		fig = use_this_fig
	if not use_pickle:
		# get data frame
		db_con = Eval_DB_Connection("db-env.ini")
		df = get_runtime_df(db_con, method, "queueing_feasible")
		for column, value in df_filter.items():
			df = df.loc[df[column] == value]
		db_con.close_connection()
		dbg_1 = df.loc[(df['executed'] == 1) & (df['runtime'].notnull())]
		dbg_2 = df.loc[(df['successful'] == 1)]
		# do data mangling
		max_ls = max(df['no_late_streams'])
		if no_bins == 0:
			no_bins = max_ls
		max_runtime = max(df['runtime'])
		# establish bins
		bins, bins_str = establish_bins(max_ls, no_bins)
		# start binning
		df_bins = []
		for no_late_streams in df['no_late_streams']:
			# find correct bin
			for idx, bin in enumerate(bins):
				if int(no_late_streams / (bin + 0.5)) == 0:
					df_bins.append(bins[idx])
					break
		df['bin'] = df_bins

		binned_dfs = [df.loc[df['bin'] == bin] for bin in bins]
		times = [[] for bin in bins]
		ratio_solved = [[] for bin in bins]
		total_stream_sets = [len(df_binned.loc[(df_binned['executed'] == 1) & (df_binned['runtime'].notnull())].index)
							 for
							 df_binned in binned_dfs]
		print("Max Time: " + str(max_runtime))
		if not isnan(max_runtime):
			for time in range(0, int(np.ceil(max_runtime)) + 1):
				if time % 100 == 0:
					print(f"Time: {time}")
				for idx, bin in enumerate(bins):
					# skip bin if empty
					if total_stream_sets[idx] == 0:
						continue
					tmp = len(
						binned_dfs[idx].loc[
							(binned_dfs[idx]['successful'] == 1) & (binned_dfs[idx]['runtime'] <= time)].index)
					times[idx].append(time)
					ratio_solved[idx].append(tmp / total_stream_sets[idx])
		with open(pickle_filename, "wb") as f:
			pickle.dump((times, ratio_solved, bins_str), f)
	for idx, time in enumerate(times):
		if not time:
			continue
		ax.plot(times[idx], ratio_solved[idx], label=bins_str[idx])
		ax.set_ylim(bottom=-0.05, top=1.05)

	# for idx, bin in enumerate(bins):
	# 	df_binned = df.loc[df['bin'] == bin]
	# 	times = []
	# 	ratio_solved = []
	# 	total_stream_sets = len(df_binned.loc[(df_binned['executed'] == 1) & (df_binned['runtime'].notnull())].index)
	# 	if total_stream_sets == 0:
	# 		continue
	# 	for time in range(0, int(np.ceil(max_runtime)) + 1):
	# 		tmp = len(df_binned.loc[(df_binned['successful'] == 1) & (df_binned['runtime'] <= time)].index)
	# 		times.append(time)
	# 		ratio_solved.append(tmp / total_stream_sets)
	# 	ax.plot(times, ratio_solved, label=bins_str[idx])
	if prettify:
		fig, ax = prettify_plot(fig, ax, 'Scheduling Runtime [s]', 'Ratio of Solved Stream Sets',
								"Ratio of Solved Stream Sets", legend=legend)
	if not use_this_ax:
		show_finished_plot()
	else:
		return ax


def plot_runtime(xparam):
	"""
	Plots the runtime of a given scheduler.
	:return:
	"""
	db_con = Eval_DB_Connection("db-env.ini")
	fig, ax = plt.subplots(nrows=1, ncols=7)
	df_filter = []
	for idx, scheduler in enumerate(scheduler_table_mapping.keys()):
		axis_font = {'size': '18'}
		df = get_runtime_df(db_con, scheduler, "queueing_feasible")
		plt_dict = OrderedDict()
		if xparam == 'streams':
			# do number of streams boxplots
			for _, df_streams in df.groupby(['no_streams']):
				plt_dict[int(df_streams.iloc[0]['no_streams'])] = df_streams['runtime'].loc[df_streams['runtime'].notnull()]
			ax[idx].boxplot(plt_dict.values())
			ax[idx].set_xticklabels(plt_dict.keys(), usetex=True, **axis_font)
			ax[idx].set_xlabel(escape_latex('Size of Stream Set'), **axis_font)
			# do number of switches boxplots
			plt_dict.clear()
		elif xparam == 'switches':
			for _, df_streams in df.groupby(['switches']):
				plt_dict[int(df_streams.iloc[0]['switches'])] = df_streams['runtime'].loc[df_streams['runtime'].notnull()]
			ax[idx].boxplot(plt_dict.values())
			ax[idx].set_xticklabels(plt_dict.keys(), usetex=True, **axis_font)
			ax[idx].set_xlabel(escape_latex('\# Switches'), **axis_font)
			# do cycle boxplots
			plt_dict.clear()
		elif xparam == 'cycle':
			for _, df_streams in df.groupby(['cycle']):
				plt_dict[int(df_streams.iloc[0]['cycle'] / 1000000)] = df_streams['runtime'].loc[
					df_streams['runtime'].notnull()]
			ax[idx].boxplot(plt_dict.values())
			ax[idx].set_xticklabels(plt_dict.keys(), usetex=True, **axis_font)
			ax[idx].set_xlabel(escape_latex('Cycle Time [ms]'), **axis_font)
		axis_font = {'size': '16'}
		ax[idx].set_title(escape_latex(scheduler_name_mapping[scheduler]), usetex=True, **axis_font, pad=15)
		if idx <= 2:
			ax[idx].set_ylim(bottom=-100, top=80000)
		else:
			ax[idx].set_ylim(bottom=-10, top=1300)
	ax[0].set_ylabel(escape_latex("runtime [s]"), usetex=True, **axis_font)
	db_con.close_connection()
	fig.suptitle('Runtimes', usetex=True)
	plt.subplots_adjust(left=0.07, right=0.95, wspace=0.55)
	for a in ax.flatten():
		a.tick_params(axis='both', which='major', labelsize=18)
	fig.set_size_inches(cm2inch(50.0), cm2inch(30.9))
	plt.show()


def get_runtime_df(db_con: Eval_DB_Connection, scheduler, feasible_type):
	"""
	For a given scheduler and feasible type, return a dataframe containing scheduler information, feasible parameters
	and topology paramters. Selects only executed feasibles.
	:return:
	"""
	scheduler_table = get_scheduler_table(scheduler)
	feasible_table = get_feasible_table(feasible_type)
	if feasible_type == "queueing_feasible":
		sql_query = f"SELECT {scheduler_table}.executed, {scheduler_table}.runtime, {scheduler_table}.successful, {feasible_table}.id, " \
					f"{feasible_table}.cycle, {feasible_table}.cut_through, {feasible_table}.no_streams, " \
					f"{feasible_table}.no_late_streams, {feasible_table}.topo_id, topologies.switches " \
					f"FROM {scheduler_table}, {feasible_table}, topologies " \
					f"WHERE {scheduler_table}.feasible_id = {feasible_table}.id " \
					f"AND {feasible_table}.executed = 1 " \
					f"AND {scheduler_table}.topo_id = topologies.id "
	else:
		sql_query = f"SELECT {scheduler_table}.executed, {scheduler_table}.runtime, {scheduler_table}.successful, {feasible_table}.id, " \
					f"{feasible_table}.cycle, {feasible_table}.max_flows, " \
					f"{feasible_table}.topo_id, topologies.switches " \
					f"FROM {scheduler_table}, {feasible_table}, topologies " \
					f"WHERE {scheduler_table}.feasible_id = {feasible_table}.id " \
					f"AND {feasible_table}.executed = 1 " \
					f"AND {scheduler_table}.topo_id = topologies.id "
	if scheduler_table == "tracing_tasks":
		sql_query += f'AND {scheduler_table}.method = "{get_scheduler_method(scheduler)}"'
	elif scheduler_table == "jssp_tasks":
		sql_query += f'AND {scheduler_table}.routing = "{get_scheduler_method(scheduler)}"'
	task_entries = db_con.get_query_from_table(sql_query, None)
	return pd.DataFrame(task_entries)


def plot_scheduler_statistics_pretty():
	db_con = Eval_DB_Connection("db-env.ini")
	# 				stream set size
	# topology size					104					504					1008
	# 					100			scheduler 1, ...	scheduler 1, ...	scheduler 1, ...
	# 					500			scheduler 1, ...	scheduler 1, ...	scheduler 1, ...
	# 					1000		scheduler 1, ...	scheduler 1, ...	scheduler 1, ...
	ratio_result_tensor = np.ndarray((7, 3, 3))
	value_idx_map = {
		104: 0,
		504: 1,
		1008: 2,
		100: 0,
		500: 1,
		1000: 2
	}
	# calc data
	x = []
	y = []
	ratios = []
	x_offset = 35
	y_offset = 55
	for idx, scheduler in enumerate(scheduler_table_mapping.keys()):
		df = get_runtime_df(db_con, scheduler, "queueing_feasible")
		for grouped_by, df_grouped in df.groupby(['no_streams', 'switches']):
			# grouped_by[0]: no_streams, grouped_by[1]: switches
			try:
				ratio_result_present = len(
					df_grouped.loc[(df_grouped['runtime'].notnull()) & (df_grouped['executed'] == 1)].index) / len(
					df_grouped.loc[df_grouped['executed'] == 1].index)
			except ZeroDivisionError:
				ratio_result_present = 0
			ratio_result_tensor[idx, value_idx_map[grouped_by[0]], value_idx_map[grouped_by[1]]] = ratio_result_present
			if idx == 0:
				x_value = grouped_by[0] - x_offset
				y_value = grouped_by[1] + y_offset
			elif idx == 1:
				x_value = grouped_by[0]
				y_value = grouped_by[1] + y_offset
			elif idx == 2:
				x_value = grouped_by[0] + x_offset
				y_value = grouped_by[1] + y_offset
			elif idx == 3:
				x_value = grouped_by[0] - x_offset
				y_value = grouped_by[1]
			elif idx == 4:
				x_value = grouped_by[0]
				y_value = grouped_by[1]
			elif idx == 5:
				x_value = grouped_by[0] + x_offset
				y_value = grouped_by[1]
			else:
				x_value = grouped_by[0] - x_offset
				y_value = grouped_by[1] - y_offset
			x.append(x_value)
			y.append(y_value)
			ratios.append(ratio_result_present)
	# plot data
	fig = plt.figure(constrained_layout=True)
	gs = fig.add_gridspec(1, 15)
	ax_1 = fig.add_subplot(gs[0, :14])
	cmap = plt.get_cmap("RdYlGn")
	ax_1.scatter(x[:9], y[:9], c=cmap(ratios[:9]), edgecolors="Black", s=400, marker="s", label="TSN SMT")
	ax_1.scatter(x[9:18], y[9:18], c=cmap(ratios[9:18]), edgecolors="Black", s=400, marker="D", label="JSSP: Shortest Path Routing")
	ax_1.scatter(x[18:27], y[18:27], c=cmap(ratios[18:27]), edgecolors="Black", s=400, marker="o", label="JSSP: Precalculated Routes")
	ax_1.scatter(x[27:36], y[27:36], c=cmap(ratios[27:36]), edgecolors="Black", s=400, marker="^", label="TBS: naive")
	ax_1.scatter(x[36:45], y[36:45], c=cmap(ratios[36:45]), edgecolors="Black", s=400, marker="<", label="TBS: Link Time Rem. Time")
	ax_1.scatter(x[45:54], y[45:54], c=cmap(ratios[45:54]), edgecolors="Black", s=400, marker=">", label="TBS: Late Streams")
	ax_1.scatter(x[54:63], y[54:63], c=cmap(ratios[54:63]), edgecolors="Black", s=400, marker="v", label="TBS: MCTS")
	ax_1.set_xlabel("Number of Streams", usetex=True)
	ax_1.set_ylabel("Number of Switches in Topology", usetex=True)
	ax_1.set_title("Scheduler Execution Rate", usetex=True)
	ax_1.set_xticks([100, 500, 1000])
	ax_1.set_yticks([104, 504, 1008])
	ax_2 = fig.add_subplot(gs[0, 14])
	cb = mpl.colorbar.ColorbarBase(ax_2, cmap=cmap, orientation='vertical')
	cb.set_label("Execution Rate")
	plt.subplots_adjust(left=0.08, right=0.95, top=0.85, bottom=0.08)
	ax_1.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left", mode="expand", borderaxespad=1.5, ncol=3, prop={'size': 15})
	leg = ax_1.get_legend()
	for handle in leg.legendHandles:
		handle.set_color("Black")
		handle.set_sizes([150])
	plt.show()


def plot_scheduler_statistics(df_filter):
	db_con = Eval_DB_Connection("db-env.ini")
	axis_font = {'size': '12'}
	plt_dict = OrderedDict()
	for idx, scheduler in enumerate(scheduler_table_mapping.keys()):
		df = get_runtime_df(db_con, scheduler, "queueing_feasible")
		for column, value in df_filter.items():
			df = df.loc[df[column] == value]
		ratio_result_present = len(df.loc[(df['runtime'].notnull()) & (df['executed'] == 1)].index) / len(
			df.loc[df['executed'] == 1].index)
		plt_dict[scheduler] = ratio_result_present
	# ax[idx].set_xticklabels(plt_dict.keys(), usetex=True, **axis_font)
	# ax[idx].set_xlabel(escape_latex('Size of Stream Set'), **axis_font)
	# ax[idx].set_title(escape_latex(scheduler_name_mapping[scheduler]), usetex=True, **axis_font)
	db_con.close_connection()
	fig, ax = plt.subplots()
	x = np.arange(len(plt_dict))
	ax.bar(x, plt_dict.values(), color='grey')
	title = "Ratio of Faultless Scheduling "
	if not df_filter:
		title += "- Filter: None"
	else:
		for f, v in df_filter.items():
			title += f"- {f}: {v} "
	ax.set_title(escape_latex(title), usetex=True, **axis_font)
	ax.set_ylabel(escape_latex("runtime [s]"), usetex=True, **axis_font)
	plt.xticks(x, [scheduler_name_mapping[scheduler] for scheduler in plt_dict.keys()], usetex=True, **axis_font)
	ax.set_ylim(bottom=0, top=1.1)
	fig.set_size_inches(cm2inch(50.0), cm2inch(30.9))
	plt.show()


def get_flow_span(packet_information_json_txt):
	"""
	Gets a packet information json and return the flowspan.
	"""
	packet_information_json = json.loads(packet_information_json_txt)
	# with open(packet_information_json_txt, "r") as f:
	# 	packet_information_json = json.load(f)
	# select send and receive events from dict
	packet_information_json_tx_rx = {key: value for key, value in packet_information_json.items() if len(value) == 2}
	min_flow_tx = min(packet_information_json_tx_rx.values(), key=lambda x: x[0][0])
	max_flow_rx = max(packet_information_json_tx_rx.values(), key=lambda x: x[1][0])
	return max_flow_rx[1][0] - min_flow_tx[0][0]


def add_flow_span(df):
	"""
	Takes a dataframe containing packet_informations dicts and adds a column containing the flowspans.
	"""
	flowspans = [get_flow_span(packet_information_json_txt) for packet_information_json_txt in
				 df['packet_information_json']]
	df['flowspan'] = flowspans
	return df


def plot_flowspans():
	# get data frame
	db_con = Eval_DB_Connection("db-env.ini")
	df = get_d3_files(db_con, "tracing_link_time_mcts", "queueing_feasible")
	db_con.close_connection()
	df = add_flow_span(df)
	fig, ax = plt.subplots(nrows=1, ncols=1)


def get_d3_files(db_con: Eval_DB_Connection, scheduler, feasible_type):
	"""
	Returns a dataframe containing the conversion tool xml files, feasible and topology parameters for a given scheduler
	and feasible type. Rows in the sim_gen_tasks table are automatically executed.
	"""
	scheduler_table = get_scheduler_table(scheduler)
	scheduler_method = get_scheduler_method(scheduler)
	simgen_type = get_simgen_type(scheduler)
	feasible_table = get_feasible_table(feasible_type)
	if feasible_type == "queueing_feasible":
		sql_query = f"SELECT d3_results.id AS d3_id, d3_results.packet_information_json, d3_results.gate_states_json, {feasible_table}.id, " \
					f"{feasible_table}.cycle, {feasible_table}.cut_through, {feasible_table}.no_streams, " \
					f"{feasible_table}.no_late_streams, {feasible_table}.topo_id, topologies.switches " \
					f"FROM {scheduler_table}, {feasible_table}, topologies, d3_results " \
					f"WHERE d3_results.feasible_id = {scheduler_table}.feasible_id " \
					f"AND d3_results.feasible_id = {feasible_table}.id " \
					f"AND {scheduler_table}.topo_id = topologies.id "
	else:
		sql_query = f"SELECT d3_results.id AS d3_id, d3_results.packet_information_json, d3_results.gate_states_json, {feasible_table}.id, " \
					f"{feasible_table}.cycle, {feasible_table}.max_flows, " \
					f"{feasible_table}.topo_id, topologies.switches " \
					f"FROM {scheduler_table}, {feasible_table}, topologies, d3_results " \
					f"WHERE d3_results.feasible_id = {scheduler_table}.feasible_id " \
					f"AND d3_results.feasible_id = {feasible_table}.id " \
					f"AND {scheduler_table}.topo_id = topologies.id "
	if simgen_type == "tracing":
		sql_query += f'AND {scheduler_table}.method = "{scheduler_method}"'
	elif simgen_type == "jssp":
		sql_query += f'AND {scheduler_table}.routing = "{scheduler_method}"'
	task_entries = db_con.get_query_from_table(sql_query, None)
	return pd.DataFrame(task_entries)


def cm2inch(value):
	return value / 2.54


def prettify_plot(fig, ax, x_label, y_label, title, font_large=True, legend=True):
	"""
	Prettifies the plot so that it can be inserted into thesis.
	:return:
	"""
	plt.rcParams['text.usetex'] = True
	plt.rcParams.update({'font.size': 30})
	fig.subplots_adjust(left=0.08, bottom=0.11, top=0.93, right=0.95)
	if legend:
		ax.legend(loc='upper right')  # bbox_to_anchor=(1.0, 1.1)
	if not font_large:
		axis_font = {}
	else:
		axis_font = {'size': '40'}
	ax.set_xlabel(x_label, usetex=True, **axis_font)
	# if not args.parameter:
	# 	ax.xaxis.set_label_coords(0.5, -0.1)
	ax.set_title(title, usetex=True, **axis_font)
	ax.tick_params(labelsize='large')
	ax.set_ylabel(y_label, usetex=True, **axis_font)
	ax.set_xlabel(x_label, usetex=True, **axis_font)
	# the aspect ratio of 50:30.9 is based on the golden ratio, it will produce nice-looking diagrams
	fig.set_size_inches(cm2inch(50.0), cm2inch(30.9))
	return fig, ax


# TODO calculate flowspan from flow.xml files so that all schedulers can be analysed
# TODO plement method to calc link utilization


def main(args):
	# build filter
	# df_filter = {}
	# for idx, _ in enumerate(args.filter):
	# 	if args.filter[idx] == "switches":
	# 		df_filter['']
	df_filter = {args.filter[idx]: args.filter_value[idx] for idx, _ in enumerate(args.filter)}
	if args.analysis == "runtime":
		plot_runtime(args.xparam)
	elif args.analysis == "stats":
		# plot_scheduler_statistics(df_filter)
		plot_scheduler_statistics_pretty()
	elif args.analysis == "solved":
		if args.scheduler != "all":
			plot_solved_stream_sets(args.scheduler, df_filter, no_bins=args.bins, legend=False)
		else:
			plot_solved_stream_sets_all(df_filter, no_bins=args.bins)
	elif args.analysis == "flowspans":
		# plot_flowspans()
		pass


if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description="Analyze results from remote mariadb in benchmarking tool.")
	parser.add_argument('analysis', type=str, help="Which analysis to do.",
						choices=["runtime", "solved", "stats"])
	parser.add_argument('-f', '--filter', nargs='+', type=str, help='For which column to filter the dataframe.', default=[])
	parser.add_argument('-v', '--filter_value', nargs='+', type=int, help='For which value to filter the column.', default=[])
	subparsers = parser.add_subparsers(description="Solved analysis requires additional arguments.", dest="subcommand")
	# which x parameter to plot for runtime analysis
	parser_xparam = subparsers.add_parser("xparam")
	parser_xparam.add_argument("xparam", type=str, choices=["streams", "switches", "cycle"],
							   help="Which parameter to plot in x-axis.")
	# which scheduler to plot for solved analysis
	parser_scheduler = subparsers.add_parser("scheduler")
	parser_scheduler.add_argument("scheduler",
								  type=str,
								  choices=["tsn_smt",
										   "jssp_shortest_path",
										   "jssp_precalculated",
										   "tracing_naive",
										   "tracing_link_time_ls",
										   "tracing_link_time_rem_time",
										   "tracing_link_time_mcts",
										   "all"],
								  help="Which analysis to run.")
	parser_scheduler.add_argument("-b", "--bins", type=int, help="Number of bins for aggregation of results.",
								  default=0)
	parser_scheduler.add_argument('-f', '--filter', nargs='+', type=str, help='For which column to filter the dataframe.', default=[])
	parser_scheduler.add_argument('-v', '--filter_value', nargs='+', type=int, help='For which value to filter the column.', default=[])
	args = parser.parse_args()
	# args = argparse.Namespace(analysis='stats', xparam='streams', bins=0, scheduler='all', subcommand='scheduler',
	# 						  filter=['no_streams'], filter_value=[1000])
	print(args)
	main(args)
