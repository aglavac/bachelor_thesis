import json


def add_is_switch(topology_path):
    """
    Adds IsSwitch property to .json graph
    :param topology_path: .json graph
    """
    with open(topology_path, 'r') as topology_fd:
        topology_data = json.load(topology_fd)

    for v in topology_data['vertices']:
        if 's' in v:
            topology_data['vertices'][v]['IsSwitch'] = True
        else:
            topology_data['vertices'][v]['IsSwitch'] = False

    with open(topology_path, 'w') as topology_fd:
        json.dump(topology_data, topology_fd)


add_is_switch('cut_through_verific/test_topology_2.json')
