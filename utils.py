import json
import os
from collections import namedtuple
from math import ceil

import graph_tool.all as gt
import networkx as nx
import xml.etree.ElementTree
import matplotlib.pyplot as plt
import pygraphviz as pgv

import typing

from tsnutils.topology.topology import parse_topology


class Vertex(namedtuple('Vertex', ['name', 'processing_delay', 'gw_cluster_id', 'IsSwitch'])):
    __slots__ = ()

    def __new__(cls, name, processing_delay, gw_cluster_id, IsSwitch):
        return super().__new__(cls, name, int(processing_delay), gw_cluster_id, IsSwitch)

    def __str__(self):
        return self.name


class Edge(namedtuple('Edge', ['src', 'dst', 'link_speed', 'propagation_delay'])):
    __slots__ = ()

    def __new__(cls, src, dst, link_speed, propagation_delay):
        return super().__new__(cls, src, dst, int(link_speed), int(propagation_delay))

    def __str__(self):
        return '{}_{}'.format(self.src, self.dst)


Stream = namedtuple('Stream', ['id', 'src', 'dst', 'ed', 'la', 'e2e', 'size', 'cycle_time'])
ModelSize = namedtuple('ModelSize', ['vertices', 'edges', 'constraints', 'variables'])


def render_tree(root):
    node_id = 0
    tree = nx.Graph()
    tree.add_node(node_id, rem_time=root.rem_time)
    stack = []
    stack.insert(0, (root, node_id))
    node_id += 1
    while stack:
        n, n_id = stack.pop()
        for c in n.children:
            stack.insert(-1, (c, node_id))
            tree.add_node(node_id, rem_time=c.rem_time)
            tree.add_edge(n_id, node_id)
            node_id += 1
    pgv_tree = nx.nx_agraph.to_agraph(tree)
    # pgv_tree.node_attr['style']='filled'
    # pgv_tree.node_attr['color']='grey'
    pgv_tree.node_attr['shape']='circle'
    # pgv_tree.node_attr['fixedsize']='true'
    pgv_tree.node_attr['fontsize']='25'
    pgv_tree.node_attr['fontcolor']='#000000'
    for n in pgv_tree.nodes():
        # n.attr['label'] = n.attr['rem_time']
        n.attr['label'] = ""
    pgv_tree.layout(prog='dot')
    print(pgv_tree.string())
    pgv_tree.draw('test.pdf')


def parse_streams(stream_path, vertices):
    streams = []

    vertex_names = [vertex.name for vertex in vertices]
    vertex_dict = dict([(vertex.name, vertex) for vertex in vertices])

    with open(stream_path) as streams_fd:
        data = json.load(streams_fd)

        for f in data:
            # Sanity check
            if f['src'] in vertex_names and f['dst'] in vertex_names:
                f['id'] = len(streams)
                streams.append(Stream(f['id'], vertex_dict[f['src']], vertex_dict[f['dst']], f['ed'], f['la'], f['e2e'],
                                      f['size'], f['cycle_time']))
            else:
                print('{} or {} not found in vertices'.format(f['src'], f['dst']))
    return streams


def calc_inout_edges(vertices, edges):
    edges_in = {}
    edges_out = {}

    for v in vertices:
        edges_in[v] = []
        edges_out[v] = []

    for edge in edges:
        edges_in[edge.dst].append(edge)
        edges_out[edge.src].append(edge)

    return edges_in, edges_out


def get_gateway_nodes(g):
    """
    :param g: graph to search for gateway nodes (gateway node is a node from the main ring that connects to a subring
    :return: dict of gateway ids and the corresponding gateway node
    """
    # gateway_nodes = dict()
    # iterate over all nodes
    # for node in g.nodes:
    #     # get node if more neighbours than 3 -> connects to another ring
    #     if g.degree[node] > 3:
    #         gateway_nodes[g.nodes[node]['gw_cluster_id']] = node
    #         # # iterate over all neighbours
    #         # for n in g.neighbors(node):
    #         #     # get node that connects to n -> this is gateway
    #         #     if g.nodes[node]['gw_cluster_id'] != g.nodes[n]['gw_cluster_id']:
    #         #         gateway_nodes[g.nodes[node]['gw_cluster_id']] = n

    gateway_nodes = {g.nodes[node]['gw_cluster_id']: node for node in g.nodes
                     if g.degree(node) == 3  # Degree must be three
                     and all(
            map(lambda node: node.startswith('s'), g.neighbors(node)))  # all neighbors must be switches
                     and g.nodes[node] and g.nodes[node]['gw_cluster_id'] != 0}  # gateway may not be in backbone
    return gateway_nodes


def extract_subdirectory(dir):
    self_folder = os.getcwd() + '/'
    local_streams_folders = [each for each in os.listdir(self_folder) if dir in each]
    return local_streams_folders


def calc_transmission_delay(link_speed, size):
    return size * 8 / (link_speed * 1e-3)


def read_topology(topology_path):
    """
    Reads toplogy from path. Takes .json and .graphml as formats. Returns graph-tool graph.
    """
    if type(topology_path) is gt.Graph:
        topology = topology_path
    else:
        # try for json input
        with open(topology_path) as topology_fh:
            try:
                json.load(topology_fh)
                topology = parse_json_to_gt(topology_path)
            except json.decoder.JSONDecodeError as e:
                pass
            topology_fh.seek(0)  # resetting file cursor
            # try for graphml input
            try:
                topology = convert_nx_to_gt(nx.read_graphml(topology_path))
            except xml.etree.ElementTree.ParseError:
                pass
    if topology:
        return parse_time_units_to_sim(topology)
    else:
        print("Topology in file <{}> could not be read!".format(topology_path))


def parse_json_to_gt(topology_path: str) -> gt.Graph:
    """
    Parses .json representation to graph-tool graph.
    """

    return


def get_str_type(type_t_c):
    tmp = str(type(type_t_c)).replace("<class ", "").replace(">", "").replace("\'", "")
    if tmp == "str":
        tmp += "ing"
    return tmp


def get_name_to_id_dict(topology: gt.Graph):
    """
    Builds mapping of verties names to graph-tool ids, to avoid calling find_vertex(), since it is slow.
    """
    vertex_dict = {}
    v_id = topology.vertex_properties['v_id']
    for v in topology.get_vertices():
        vertex_dict[v_id[v]] = v
    return vertex_dict


