#!/usr/bin/env python3
"""
This file contains all necessary method to convert scheduling-benchmarking files to input for tracing_based_scheduling.
"""

import argparse
import json

import networkx as nx

from stream_sets.generate_streams import convert_streams
from stream_sets.generate_streams import merge_streams_routes
from utils import read_topology


def rename_topology_attributes(topology_path, output_file=True):
	topology_gt = read_topology(topology_path)

	proc_delay = topology_gt.new_vertex_property('int')
	proc_delay_unit = topology_gt.new_vertex_property('string')
	is_switch = topology_gt.new_vertex_property('bool')
	topology_gt.vp['ProcessingDelay'] = proc_delay
	topology_gt.vp['TimeUnit'] = proc_delay_unit
	topology_gt.vp['IsSwitch'] = is_switch
	# add additional node attributes with correct name
	for n in topology_gt.vertices():
		proc_delay[n] = topology_gt.vp['processing_delay'][n]
		proc_delay_unit[n] = 'ns'
		is_switch[n] = topology_gt.vp['is_switch'][n]
	prop_delay = topology_gt.new_edge_property('int')
	datarate = topology_gt.new_edge_property('int')
	prop_delay_unit = topology_gt.new_edge_property('string')
	datarate_unit = topology_gt.new_edge_property('string')
	topology_gt.ep['Datarate'] = datarate
	topology_gt.ep['PropagationDelay'] = prop_delay
	topology_gt.ep['DatarateUnit'] = datarate_unit
	topology_gt.ep['TimeUnit'] = prop_delay_unit
	# do same for edges
	for e in topology_gt.edges():
		datarate[e] = topology_gt.ep['link_speed'][e]
		datarate_unit[e] = 'Mbps'
		prop_delay[e] = topology_gt.ep['propagation_delay'][e]
		prop_delay_unit[e] = 'ns'
	if output_file:
		write_gt_to_json(topology_gt, topology_path)
	else:
		return topology_gt


def convert_files(argmnts):
	merge_streams_routes(argmnts.streams_path, argmnts.routes_path)
	convert_streams(argmnts.streams_path, argmnts.cycle_time, 1.1)
	rename_topology_attributes(argmnts.topology_path)
	# rename_nodes(argmnts.topology_path, argmnts.streams_path)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--topology_path", type=str, required=True,
						help="Path to topology file.")
	parser.add_argument("--streams_path", type=str, required=True,
						help="Path to streams.json file.")
	parser.add_argument("--routes_path", type=str, required=True, help="Path to routes.json file.")
	parser.add_argument("--cycle_time", type=int, required=True, help="Cycle time [mu s] of streams.")
	args = parser.parse_args()
	# args = argparse.Namespace(topology_path="dbg/topology.json", streams_path="dbg/streams_feasible.json", routes_path="dbg/routes_feasible.json", cycle_time=1000)
	convert_files(args)
